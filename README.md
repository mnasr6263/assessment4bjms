# Docker & Kubernetes Assessment 4
---
These instructions will get you a copy of the assessment 4 project up and running on your local machine.

---
### Links to Working Apps

[Trello Board](https://trello.com/b/fvMK8Z4C/assessment-4)

[Jenkins server](http://bajams-jenkins.academy.labs.automationlogic.com/)

[Static Website](http://bajams-static.academy.labs.automationlogic.com/)

[PetClinic Application](http://bajams-petclinic.academy.labs.automationlogic.com/)

[Wordpress Application](http://bajams-wordpress.academy.labs.automationlogic.com/)

---

##  <a name='TableofContents'></a>Table of Contents

<!-- vscode-markdown-toc -->
1. [Introduction](#Introduction)
2. [How to Deploy Guide](#Instructions)
3. [Infrastructure](#Infrastructure)
4. [Jenkins](#Jenkins)
      
    4.1 [Infrastructure Setup Pipeline](#InfraPipeline)
        
      - 4.1.1. [Terraform_setup](#Terrasetup)

      - 4.1.2 [Kubernetes_Installation](#Kubeinstall)
  

    4.2 [Deployment Pipeline](#DPipeline)

    - 4.2.1. [BaJaMS_clone_k8_setup](#Gitclone)
  
    - 4.2.2. [Deploy_PetClinic](#Dpetclinic)
  
    - 4.2.3. [Deploy_Wordpress](#Dwordpress)
  
    - 4.2.4. [Deploy_Static](#Dstatic)


    4.3 [Development Pipeline+ Deployment of development code ](#DevPipeline)

    - 4.3.1. [DEV_test_petclinic](#Devpetclinic)
  
    - 4.3.2. [DEV_test_static](#Devstatic)
  
    - 4.3.3. [push_petclinic_ECR](#ECR)

    - 4.3.4. [push_static_app_kubernetes and push_petclinic_app_kubernetes](#PushApp)
   
    4.4 [New Wordpress Blogs](#NewBlog)

    - 4.4.1 [Wordpress_New_Blog](#NewBlog)
      

5. [Docker Images](#Docker)
   
    5.1 [Using AWS Elastic Container Registry](#ECR)

    - 5.1.1 [Setting up a new repo](#ECRrepo)
    
    5.2 [Docker Images](#DockerImg)

    - 5.2.1 [Pet Clinic](#pcImg)
  
    - 5.2.2 [Static Webpage](#staticImg)


6. [Kubernetes Cluster](#Kubernetes)
7. [RDS Restore](#RDS)
8. [Slack Notifications](#Slack)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

---

##  1. <a name='Introduction'></a>Introduction

This is a walkthrough of how to launch a functioning PetClinic and Wordpress application via a CI/CD pipeline on Jenkins. 

Focusing on the creation of an automated workflow on the following core architecture:

- Infrastructure as code deployment using Terraform.

- A Jenkins server functioning a CI/CD pipeline. 
  
- Resilience, Repetablily and Seamless Change.
  
- Use of Docker images and containers.

- Kubernetes for Deployment and Scalability.
  
- Continuous delivery in the testing of code.

- [Testing] - Slack notifications after each build/job.

### Architecture Diagram
![Diagram](./z_readme_images/Assessment4.png)


As per the assessment 4 brief; Docker and Kubernetes was used to build and deploy Petclinic, Wordpress and a static website. 

---

##  2. <a name='Instructions'></a>How to Deploy

1. Log in to Jenkins (username and passwords have been sent to you via Slack).
2. Click on `1.Infrastructure_setup` and click build now on `Terraform_setup` job.
   - This will trigger a pipeline to build the infrastructure for this project. Further details below. The order in which the jobs are run is listed below in the **Jenkins** section in the readme. Further details below.
3. You will have a working petclinic, wordpress and a static website all configured using kubernetes.
   
We also have a dev environment that is connected to the dev branches on the repository, if you'd like to test this jenkins pipeline (`3.Dev`), go to the **Developement Pipeline** section in the readme.

* *If you want to create a fresh Jenkins Master and Worker instances,  uncomment the .tf files in the **jenkins folder within the modules folder** and uncomment the **jenkins module in main.tf** within the Terraform folder. Then run `terraform init` followed by `terraform apply -target=module.jenkins -auto-approve`* 

**Make sure you have terraform installed on whichever machine you want to run this on.**
  
* *If you want to have a fresh installation of the infrastructure, run the terraform destory job on Jenkins `5.Management` followed by steps listed above.*


---

##  3. <a name='Infrastructure'></a>Infrastructure

The infrastructure of this project was built using Terraform and can be found on the repository in the **Terraform** folder, this includes:

- Jenkins Master & Worker.

- Security Groups.

- RDS databases.
  
- Kubernetes Cluster.

- Loadbalancer & target group.

- Route53 DNS names.


---

##  4. <a name='Jenkins'></a>Jenkins

Log in to Jenkins and build in the order of the development pipeline to launch Petclinic, Wordpress and static website.

[Link to go to Jenkins](http://bajams-jenkins.academy.labs.automationlogic.com/)

Jenkins has been set up to build the infrastraucture and launch the applications using a CI/CD pipeline. This is organised into two pipelines; `Infrastructure_setup` and `Deployment`.

![Jenkins dashboard](./z_readme_images/JenkinsOverview.png)

A developement environment has also been configured, providing an environment that tests any changes in the code which can then be passed into deployment. 

---

### 4.1. <a name='InfraPipeline'></a>Infrastructure Setup Pipeline

Log in to Jenkins and build in the order of the developemt pipeline to build the infrastructure and launch Petclinic, Wordpress and static website.

This part of the CI/CD pipeline uses terraform to build the infrastructure, it is divided into two jobs; `Terraform_setup` and `Kubernetes_Installation`. 


#### 4.1.1. <a name='Terrasetup'></a>Terraform_setup

In the first job, a Jenkins pipeline is used to build each section of the infrastructure. This is broken down into stages:

1. Git clone
   - This job git clones the repository into the jenkins job workspace
2. Terraform started
   - Echo's the start of the terraform build
3. Building security goups
   - A terraform apply is run targeting the security group module and builds all the security groups needed for this projects.
4. Creating RDS database
   - A terraform apply is run targeting the RDS module, creating the RDS databases and DNS names. 
5. S3 Bucket build
   - A terraform apply is run targeting the s3 module, this holds the BajamsKey.pem and sshkey password generated to create Kubernetes cluster nodes.
6. Loadbalancer build and route53 DNS names for webservers
   - A terraform apply is run targeting the loadbalancer module, creating the loadbalancer and assigns DNS names for the applications.
7. Building Kubernetes Cluster
   - A terraform apply is run targeting the k8 cluster module, this builds two master nodes and 3 worker nodes within the kubernetes cluster with git installed in each machine and kubernetes-ingress cloned in the master node.
8. Allow instances to intiallise
   - Inacts a 3 minute wait to allow builds to initialise.


![Terraform setup pipeline](./z_readme_images/terraform_build.png)


#### 4.1.2. <a name='Kubeinstall'></a>Kubernetes_Installation

The second job, use a Jenkins pipeline to configure and install the Kubernetes clusters using Kubespray. This is divided into two stages:

1. SSH into K8 nodes to pass ssh key
2. Configuration of kubespray installation

![Kubernetes installation pipeline](./z_readme_images/KubernetesBuild.png)

Similar to the manual configuration of kubernetes, a K8 controller (t2.micro, 20gb) and master/worker (k8) instances (t3a.xlarge, 30gb) are created. 

Within the K8 controller an ssh key is generated and passed through to the master/worker nodes created to allow ssh access in to these instances. 

In the K8 controller the following commands are run:
```bash
sudo yum install -y git
git clone https://github.com/kubernetes-sigs/kubespray.git
cd kubespray/

sudo pip3 install -r requirements.txt
cp -rfp inventory/sample inventory/mycluster

```

The IP address of each master/worker node made is declared via the terraform build and is added into the hosts.yaml file to configure the kubernetes cluster.

```bash
cd /home/ec2-user/kubespray
export AWS_DEFAULT_PROFILE=Academy

ansible-playbook -i /home/ec2-user/kubespray/inventory/mycluster/hosts.yaml --become --become-user=root /home/ec2-user/kubespray/cluster.yml
```

---

###  4.2. <a name='DPipeline'></a>Deployment Pipeline

This part of the CI/CD pipeline, launches the PetClinic and Wordpress applications and a static website after the infrastructure and the kubernetes clusters have been created.

**Pipeline order:**

1. BaJaMS_clone_k8_setup
2. Deploy_PetClinic
3. Deploy_Wordpress
4. Deploy_Static

The scripts run by the Jenkins jobs are found in the `jenkins_scripts` folder. 

#### 4.2.1. <a name='Gitclone'></a>BaJaMS_clone_k8_setup

This job is triggered after the configuration of the kubernetes clusters.

1. SSH's into the K8 Master node and clones the required files from this repository which are used in the deployment of the other jobs. 

2. A specified port is pinned to the haproxy ingress rule to direct the loadbalancer to the specified port. 

```bash
sudo sed -e "213 s/    targetPort: 80/    targetPort: 80\n    nodePort: 30006/" /home/ec2-user/kubernetes-ingress/deploy/haproxy-ingress.yaml > /home/ec2-user/kubernetes-ingress/deploy/haproxy.yaml
```

3. Kompose covert is installed.
   
```bash
curl -L https://github.com/kubernetes/kompose/releases/download/v1.22.0/kompose-linux-amd64 -o kompose
sudo chmod +x kompose
sudo mv kompose /usr/local/bin/kompose
```
4. Metrics server is deployed.

```bash

kubectl apply -f /home/ec2-user/assessment4bjms/metrics-server/metrics.yaml

```

A successful build triggers the deployment of the Petclinic and Wordpress app and the static website.

#### 4.2.2. <a name='Dpetclinic'></a>Deploy_PetClinic

This job is trigger after the BaJaMS_clone_k8_setup job to deploy the Petclinic application by SSH'ing to the Kubernetes Master Node.

- SSH into K8 Master node
- Creates namespace called `petclinic`
- Generate petclinic deployment, service files using `kompose convert` 
- Applies petclinic deployment, service and ingress files.
- Petclinic deployment uses the petclinic image from bajams-petclinic repository on Amazon's ECR 

If this build is successful you should be able to access the very first wordpress blog at: [PetClinic Application](http://bajams-petclinic.academy.labs.automationlogic.com/)

The following diagram displays the flow of the job:

![DEPLOYjobs](./z_readme_images/DEPLOY_jobs.png)


#### 4.2.3. <a name='Dwordpress'></a>Deploy_Wordpress

This job is triggered to build after BaJaMS_clone_k8_setup to deploy the wordpress application.

- SSH into K8 Master node
- Creates namespace called `wordpress` 
- Moves into repo and applies wordpress deployment, service and ingress files.
- Wordpress deployment uses the wordpress image from DockerHub 

If this build is successful you should be able to access the very first wordpress blog at: [Wordpress Application](http://bajams-wordpress.academy.labs.automationlogic.com/)

#### 4.2.4. <a name='Dstatic'></a>Deploy_Static
This job is trigger after the BaJaMS_clone_k8_setup job to deploy the Static Webpage application by SSH'ing to the Kubernetes Master Node.
The Deploy Static job follows a similar workflow of the Deploy Petclinic job.

- SSH into K8 Master node
- - Creates namespace called `static`
- Generate static deployment, service files using `kompose convert` 
- Applies static deployment, service and ingress files.
- Static deployment uses the static image from bajams-static repository on Amazon's ECR

If this build is successful you should be able to access the very first wordpress blog at: [Static Application](http://bajams-static.academy.labs.automationlogic.com/)

---

### 4.3. <a name='DevPipeline'></a>Development Pipeline + Deployment of development code 

#### 4.3.1. <a name='Dstatic'></a>DEV_test_petclinic

#### **Simulating a development workflow**
*Even though there is no one actually updating Pet Clinic code, as part of the deliverables there is a need to be able to simulate a developer workflow and be able to support it in the infrastructure that is being delivered. The developer workflow  for the pipeline is as follows:*
1. Developer working on Petclinic pushes new code on the dev branch: `docker-image-pc-dev`
2. Jenkins job `DEV_test_petclinic` using a webhook will respond to the commit by triggering the job
3. A new docker image will be created inside the Jenkins worker and spun up inside a container
4. The developer can then see the app running by connecting to the IP of the Jenkins worker on a browser

![Configure DB details 2](./z_readme_images/dev-pipeline.png)

<br>

**Steps to simulate workflow**
1. On the repository, `git checkcout  docker-image-pc-dev` to change to the dev branch
2. To edit: 
   - `bajams-petclinic/src/main/resources
resources/templates` for HTML
   - `resources/static/resources` for fonts and images
3. git add, commit and push changes on the branch
4. Go to Jenkins UI to see the running of jobs
5. Open the IP address for the Jenkins worker to see Pet clinic running.

<br>

#### *...but where are the tests in the testing environment...?*
-  Due to time constraints it was decided that it would not be a deliverable to write our own tests 
-  As no tests were provided by the client either, the purpose of the testing environment is then to be a sandbox environment for developers to spin up their code and play around with the app in a live server 

<br>
#### **Easy configuration to change database details:**
- The Jenkins job `DEV_test_petclinic`  is a parameterised job, meaning that through the user interface of Jenkins, the client or developers can change variables that is used when running the app in the dev environment. 
- Fulfilling the second user story above, the parameters that can be changed for the job are database credentials (username, password, database name and database end point). 
- This means that if the developer or client want to see the development environment app run on a different database than the default one set in Jenkins, they are able to do so through the interface instead of through code. 

<br>

*Configure DB details of the database used by test environment app through the Jenkins UI*
![Configure DB details 1](./z_readme_images/dev-db-1.png)

<br>

*Configure the default database details of the job when being triggered through the webhook: configure -> edit default value for corresponding data type*
![Configure DB details 2](./z_readme_images/dev-db-2.png)

<br>

#### 4.3.2. <a name='Devstatic'></a>DEV_test_static
#### **An actual development workflow**
*As the static website was actually developed by Jamaine this job was used as part of his development workflow. Similar to the Pet clinic development workflow it follows these steps:*

1. Jamaine pushes new HTML code to his repo
   
2.  It builds the new docker image and container holding the image 
   
3. Pushes it to the ECR private registry so that it is available for production
   
4. Triggers a down stream job `push_static_app_kubernetes` which runs a refresh in deployment with the new static website
<br>

#### 4.3.3. <a name='ECR'></a>push_petclinic_ECR
#### **No testing suite = Manual deployment**
*Since this project does not have a testing suite to validate whether a newly pushed Petclinic code base is suitable for production, it was decided that deployment to production should be a manual interaction done by developers.*
- In the same vein as how the Pet clinic development pipeline simulates a hypothetical workflow, this final part of development leading to deployment continues from that simulated context
- Taking into account that there are no automated tests, deployment should only be conducted through manual interaction where the developers have checked that the outcome of their new code is suitable for deployment
- **Once they have deemed the code to be suitable, they can run this manual job to push it to the ECR which will trigger `push_petclinic_app_kubernetes` as a downstream job to implement the change into the Kubernetes cluster**
- *This is not a robust way of deployment relying on human interaction to check if code is working but given the context it is the most appropriate way*
- *Future work would focus on implementing automated tests and to automate this deployment step*
<br>

#### 4.3.4. <a name='PushApp'></a> `push_static_app_kubernetes` and `push_petclinic_app_kubernetes`
#### **Deploying code from Dev to Production**
*Unlike the deployment jobs used during the initial configuration of the infrastructure, these two jobs use the `kubectl rollout restart` command to trigger a rolling update and get pods to use the new images for each app.*

- Code example for deploying Pet Clinic
```
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$bastion_ip '

keypair=BajamsKey.pem
priv_ip_master1=$(cat /home/ec2-user/master0_ip)

ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$priv_ip_master1 /bin/bash << EOF
sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf
kubectl rollout restart deploy -n petclinic
```

---
### 4.4. <a name='NewBlog'></a>New Wordpress Blogs

Wordpress only allows one blog per Route53. We have therefore created a job that, when built, will create a new namespace and run new deployment, service and ingress yaml files with a new host name. This means that every user will have their own blog with a unique Route53 DNS connected to a unique schema within the RDS.

#### 4.4.1. <a name='NewBlog'></a>Wordpress_New_Blog
 
- Similar to the deployment pipeline, the jenkins worker will SSH into the K8 Master Node.
- The wp_new.sh script is executed which creates and runs the new K8 files.
- The loop below is in the wp_new.sh script. It starts at `i=0` and checks whether the namespace wordpress`i` exists. If so it will increment the value of `i` until it returns an exit code of 1, which means it does not exist. At this point, the variable `$newname` is saved with the first iteration of `i` that does not exist.
```bash
name=wordpress
kubectl get namespace | grep "$name"
namespaceexitcode=$?
i=0
while [[ $namespaceexitcode == "0" ]]; do
   newname=$name$i
   kubectl get namespace | grep "$newname"
   namespaceexitcode=$?
   let i++
done
kubectl create namespace $newname

```
- The variable `$newname` is then used throughout the script to create a new schema in the RDS, as well as to copy and rename the K8 files/directories.
- sed commands are used to update namespace metadata, DB_NAME in the deploy.yaml and the host name in the ingress.yaml files. 
- AWS CLI commands are used to create a new Route53 record specific to the value of `$newname`.

---

##  5. <a name='Docker'></a>Docker Images & ECR

### 5.1 <a name='ECR'></a>Using AWS Elastic Container Registry

This infrastructure utilises Amazon's Elastic Container Registry to store docker images. The setup of the Registry is done manually for each application that requires a custom docker image:
- static webpage application
- petclinic application

#### 5.1.1 <a name='ECRrepo'></a>Setting up a new repo:
1. Login to the AWS management console and access ECR and click 'Get Started'
2. Once you have gotten started you will be pointed to a window similar to the one depicted below. Simply change the visibility settings of your repository (private or public based on your security) and add the repository name of the image you wish to build, e.g. 'petclinic'. ![ECRrepo](./z_readme_images/ECRrepo.png)
3. When you have configured your Dockerfile and it is ready to build, you will need AWS ECR policy permissions to be able to push/pull your docker image from your custom AWS ECR repo. The required policies will need to be added to an IAM user (for local pushes) or an IAM role that is configured with an IAM role with the policies.
   The required policies:
   - EC2InstanceProfileForImageBuilderECRContainerBuilds
   - AWSAppRunnerServicePolicyForECRAccess
4. Once IAM user/role is configured, follow these steps to push docker image:
   1. Retrieve an authentication token and authenticate your Docker client to your registry: 
    ```
    aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 242392447408.dkr.ecr.eu-west-1.amazonaws.com
   ```
   2. Build your Docker image using the following command (if your image is already built you can skip this step): 
   ```
   docker build -t <image name> .
   ```
   3. After the build completes, tag your image so you can push the image to this repoistory:
   ```
   docker tag <image name>:<tag> 242392447408.dkr.ecr.eu-west-1.amazonaws.com/<repo name>:<tag>
   ```
   4. Run the following command to push this image to your newly created AWS repository:
   ```
   docker push 242392447408.dkr.ecr.eu-west-1.amazonaws.com/<repo name>:<tag>
   ```

   If developers wish to push to an exisiting repo, access ECR and follow the 'push commands' found for the particular repo. To use existing Dev environment, access the instance `BaJaMS-ECR-Env` which has the IAM Role `ALAcademyJenkinsSuperRole` which has the policies needed to connect to ECR. This instance is used to edit the docker images for `Pet Clinic` and `Static Webpage`. By git cloning the assessment repo into the machine, edits can be made and pushed back into the repo. Developers can also push directly into the ECR repo (skipping testing) or automatic testing is initiated by Jenkins when changes are pushed to Bitbucket.

---

### <a name='DockerImg'></a>5.2 Docker Images
#### <a name='pcImg'></a>5.2.1 Pet Clinic
The pet clinic is built using docker hub images for `maven:3.6.3-jdk-11 AS compile` and java:`openjdk:9`. The use of two docker images allows for multi-stage building. By using maven as a compiler, the dependencies for pet clinic are downloaded and the pet clinic repo is downloaded from github and maven is then use to build pet clinic. The java image is then used to; 1) configure the application.properties files for petclinic using custom database information, and then run the application on container port 8080.

#### <a name='staticImg'></a>5.2.2 Static Webpage
The static webpage image is built using the docker hub image `php:apache`. Apache uses `/var/www/html/` as the main directory for displaying webpages. The Dockerfile executes `COPY` commands to add custom html pages to apache's main directory with the homepage file requiring the name `index.php`.

---

##  6. <a name='Kubernetes'></a>Kubernetes Cluster
### 6. <a name='Networking'></a>Networking in Kubernetes

**How networking is configured from outside traffic into Kubernetes pods running our apps**
- `Image 1:` A single deployment is made for each app that launches a pod running a container using an image of the application
- `Image 2:` Deployed pods are load balanced as well as exposed to other internal services by the ClusterIP service
- `Image 3:` Deployed pods are also auto-scaled by the Horizontal Pod Auto scaler based on its CPU usage. 
- `Image 4:`The ingress controller (HA Proxy Ingress controller for Kubernetes in our case) acts as an intermediary between outside traffic and the Kubernetes cluster. It sends the traffic to all the ingress rules inside the system. Ingress rules are set explicitly to look for incoming traffic from specific DNS names and to redirect them to the appropriate services which will then point that traffic to the pods running the specific apps.

<br>

![Kubernetes networking 1](./z_readme_images/K8-networking-1.png)
![Kubernetes networking 2](./z_readme_images/K8-networking-2.png)

---

##  7. <a name='RDS'></a>RDS Restore
To restore a database snapshot: 

1. Log on to Amazon's console and click on "RDS" 
2. Click "Automated backups" 
3. Click on the particular RDS instance based on the application eg. "bajams-database-pc" or "bajams-database-wp" 
4. Click on "Actions"  
5. Click on "restore to point in time"

A Replica RDS has been created acting as a failover, which in the case of the primary DB failing, will take over as the Primary.

---

##  8. <a name='Slack'></a>Slack Notifications

![Slack Notif](./z_readme_images/Slack-notif.png)