#### Deploy_Wordpress
This job is triggered to build after BaJaMS_Git_Clone to deploy the wordpress application along with petclinic and the static website.

- SSH into K8 Master node
- Creates namespace called `wordpress` 
- Moves into repo and applies wordpress deployment, service and ingress files.

If this build is successful you should be able to access the very first wordpress blog at:\
http://bajams-wordpress.academy.labs.automationlogic.com/



### 4.4 New Wordpress Blogs

Wordpress only allows one blog per Route53. We have therefore created a job that, when built, will create a new namespace and run new deployment, service and ingress yaml files with a new host name. This means that every user will have their own blog with a unique Route53 DNS connected to a unique schema within the RDS.  

#### 4.4.1 Wordpress-New-Blog 
- Similar to the deployment pipeline, the jenkins worker will SSH into the K8 Master Node.
- The wp_new.sh script is executed which creates and runs the new K8 files.
- The loop below is in the wp_new.sh script. It starts at `i=0` and checks whether the namespace wordpress`i` exists. If so it will increment the value of `i` until it returns an exit code of 1, which means it does not exist. At this point, the variable `$newname` is saved with the first iteration of `i` that does not exist.
```bash
name=wordpress
kubectl get namespace | grep "$name"
namespaceexitcode=$?
i=0
while [[ $namespaceexitcode == "0" ]]; do
   newname=$name$i
   kubectl get namespace | grep "$newname"
   namespaceexitcode=$?
   let i++
done
kubectl create namespace $newname

```
- The variable `$newname` is then used throughout the script to create a new schema in the RDS, as well as to copy and rename the K8 files/directories.
- sed commands are used to update namespace metadata, DB_NAME in the deploy.yaml and the host name in the ingress.yaml files. 
- AWS CLI commands are used to create a new Route53 record specific to the value of `$newname`