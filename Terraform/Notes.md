## Service Discovery and Monitoring 

### What is it?
Service discovery is how applications and (micro)services locate each other on a network. Implementations include both a central server(s) that maintain a global view of addresses and clients that connect to the central server to update and retrieve addresses.

### Examples?
There are two types of service discovery: Server-side and Client-side. Server-side service discovery allows clients applications to find services through a router or a load balancer. Client-side service discovery allows clients applications to find services by looking through or querying a service **registry**, in which service instances and endpoints are all within the service registry.

### How do they work?
There are three components to Service Discovery: the service provider, the service consumer and the service registry.

- The Service Provider registers itself with the service registry when it enters the system and de-registers itself when it leaves the system.

- The Service Consumer gets the location of a provider from the service registry, and then connects it to the service provider.

- The Service Registry is a database that contains the network locations of service instances. The service registry needs to be highly available and up to date so clients can go through network locations obtained from the service registry. A service registry consists of a cluster of servers that use a replication protocol to maintain consistency.

___

## Deployment of Containers

### What is it?
Container deployment is the act of pushing containers to their target environment. 

### What are some tools? (there is an aws one)

Kubernetes 
Openshift
Docker Swarm
Docker Compose

### What other features do they bring?

kubernetes - Kubernetes supports both declarative configuration and automation. It can help to automate deployment, scaling, and management of containerized workload and services.

- **Automates various manual processes:** for instance, Kubernetes will control for you which server will host the container, how it will be launched etc.
- ***Interacts with several groups of containers:** Kubernetes is able to manage more cluster at the same time
- **Provides additional** services: as well as the management of containers, Kubernetes offers security, networking and storage services
- **Self-monitoring:** Kubernetes checks constantly the health of nodes and containers
- **Horizontal scaling:** Kubernetes allows you scaling resources not only vertically but also horizontally, easily and quickly
- **Storage orchestration:** Kubernetes mounts and add storage system of your choice to run apps
- **Automates rollouts and rollbacks:** if after a change to your application something goes wrong, Kubernetes will rollback for you
- **Container balancing:** Kubernetes always knows where to place containers, by calculating the “best location” for them
- **Run everywhere:** Kubernetes is an open source tool and gives you the freedom to take advantage of on-premises, hybrid, or public cloud infrastructure, letting you move workloads to anywhere you want

openshift - It helps in the automation of applications on secure and scalable resources in hybrid cloud environments.

Docker compose - is for defining and running multi-container applications that work together. Docker-compose describes groups of interconnected services that share software dependencies and are orchestrated and scaled together

### How do they work? (brief infrastructure wise where does it fit in?)

Docker helps to “create” containers, and Kubernetes allows you to “manage” them at runtime. Use Docker for packaging and shipping the app. Employ Kubernetes to deploy and scale your app.

![kubernetes infrastraucture](https://csharpcorner.azureedge.net/article/getting-started-with-kubernetes-part2/Images/1.png)

When you use a container orchestration tool, such as Kubernetes, you will describe the configuration of an application using either a YAML or JSON file. The configuration file tells the configuration management tool where to find the container images, how to establish a network, and where to store logs.

When deploying a new container, the container management tool automatically schedules the deployment to a cluster and finds the right host, taking into account any defined requirements or restrictions. The orchestration tool then manages the container’s lifecycle based on the specifications that were determined in the compose file.

Kubernetes deployments define the scale at which you want to run your application by letting you set the details of how you would like pods replicated on your Kubernetes nodes. Deployments describe the number of desired identical pod replicas to run and the preferred update strategy used when updating the deployment. Kubernetes will track pod health, and will remove or add pods as needed to bring your application deployment to the desired state.

