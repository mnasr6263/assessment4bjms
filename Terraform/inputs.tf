variable "region" {
    default = "eu-west-1"
}

variable "key_name" {
    default = "BajamsKey"
}

variable "pc_rds_username" {
    default = "admin"
}

variable "pc_rds_password" {
    default = "assessment4!"
}

variable "wp_rds_username" {
    default = "admin"
}

variable "wp_rds_password" {
    default = "assessment4!"
}

variable "instance_type" {
    default = "t2.micro"
}

variable "instance_type_input" {
    default = "t3a.xlarge"
}

variable "subnet_1c" {
    default = "subnet-7bddfc1d"
}

variable "subnet_1a" {
    default = "subnet-a94474e1"
}

variable "subnet_1b" {
    default = "subnet-953a58cf"
}

variable "vpc_id" {
    default = "vpc-4bb64132"
}

variable "pc_env_tag" {
    description = "petclinic database name"
    default = "bajams-pc"
}

variable "wp_env_tag" {
    description = "petclinic database name"
    default = "bajams-wp"
}

variable "team_name_tag" {
    default = "BaJaMS"
}

variable "pc_database_name" {
    default = "bajams-database-pc"
}

variable "wp_database_name" {
    default = "bajams-database-wp"
}