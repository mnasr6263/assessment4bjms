output "home_ips_sg" {
    value = aws_security_group.bajams_sg_home_ips.id
}

output "database_sg" {
    value = aws_security_group.bajams_db_sg.id
}

output "loadbalancer_sg" {
    value = aws_security_group.bajams_sg_loadbalancer.id
}

output "bitbucket_sg" {
    value = aws_security_group.bajams_bitbucket.id
}

output "bastion_sg" {
    value = aws_security_group.bajams_sg_bastion.id
}

output "kubernetes_sg" {
    value = aws_security_group.kubernetes_sg.id
}

# output "eks_sg" {
#     value = aws_security_group.bajams_sg_eks.id
# }

# output "jenkins_master_sg" {
#     value = aws_security_group.bajams_jenkins_master_sg.id
# }

# output "jenkins_worker_sg" {
#     value = aws_security_group.bajams_jenkins_worker_sg.id
# }