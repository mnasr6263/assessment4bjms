/* -------------------------------------------------------------------------- */
/*                               Security Groups                              */
/* -------------------------------------------------------------------------- */

/* ------------------------ All traffic from home IPs ----------------------- */

resource "aws_security_group" "bajams_sg_home_ips" {
  name = "bajams_sg_home_ips"
  description = "Allows all traffic from BaJaMS Home IPs"
  tags = {
    "Name" = "${var.team_tag}_Home_IPs_SG"
  }
  vpc_id = var.vpc_id
  ingress {
    cidr_blocks = [ "81.152.189.117/32"]
    description = "Allows all traffic from JAMAINE"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }
  ingress {
    cidr_blocks = ["2.29.11.133/32"]
    description = "Allows all traffic from SELINA"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }
  ingress {
    cidr_blocks = ["89.211.133.106/32", "37.210.227.157/32"]
    description = "Allows all traffic from MOHAMED"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }        
  ingress {
    cidr_blocks = ["86.137.163.95/32"] # Change this everyday
    description = "Allows all traffic from BRIAN"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }    
  ingress {
    cidr_blocks = ["172.31.0.0/16"]
    description = "Allows all traffic from local VPC"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }
  egress {
    cidr_blocks = var.open_internet
    from_port = var.outbound_port
    ipv6_cidr_blocks = var.ipv6_cidr_block
    protocol = "-1"
    to_port = var.outbound_port
  }
}

/* ------------------------- Bastion security group ------------------------- */

resource "aws_security_group" "bajams_sg_bastion" {
  name = "bajams_sg_bastion"
  description = "Allows SSH from Home IPs"
  tags = {
    "Name" = "${var.team_tag}_SG_Bastion"
  }
  vpc_id = var.vpc_id
  ingress {
    security_groups = [aws_security_group.bajams_sg_home_ips.id]
    description = "Allows port 22 from Home IPs"
    from_port = var.ssh_port
    protocol = "tcp"
    to_port = var.ssh_port
  }
  ingress {
    security_groups = [var.sg_jenkins_worker]
    description = "Allows port 22 from Jenkins Worker SG"
    from_port = var.ssh_port
    protocol = "tcp"
    to_port = var.ssh_port
  }
  ingress {
    cidr_blocks = ["172.31.0.0/16"]
    description = "Allows all traffic from local VPC"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }

  egress {
    cidr_blocks = var.open_internet
    from_port = var.outbound_port
    ipv6_cidr_blocks =var.ipv6_cidr_block
    protocol = "-1"
    to_port = var.outbound_port
  }
}  

/* ----------------------------- Loadbalancer SG ---------------------------- */

resource "aws_security_group" "bajams_sg_loadbalancer" {
  name = "bajams_sg_loadbalancer"
  description = "Allows port 80 all traffic and SSH from bastion"
  vpc_id = var.vpc_id
  ingress {
    from_port = var.http_port
    to_port = var.http_port
    protocol = "tcp"
    cidr_blocks = var.open_internet
    description = "Allows port 80"
    }
  ingress {
    cidr_blocks = ["172.31.0.0/16"]
    description = "Allows all traffic from local VPC"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
    }

  egress {
    from_port = var.outbound_port
    to_port = var.outbound_port
    protocol = "-1"
    cidr_blocks = var.open_internet
    ipv6_cidr_blocks = var.ipv6_cidr_block
  }
  tags = {
    "Name" = "${var.team_tag}_Loadbalancer_SG"
  }
} 

/* --------------------------- RDS Security Group --------------------------- */

resource "aws_security_group" "bajams_db_sg" {
  name = "bajams_db_sg"
  description = "Allows port 3306"
  vpc_id = var.vpc_id
  ingress {
    cidr_blocks = ["172.31.0.0/16"]
    description = "Allows all traffic from local VPC"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }
  ingress {
    from_port = var.mysql_port
    to_port = var.mysql_port
    protocol = "tcp"
    security_groups = [aws_security_group.bajams_sg_home_ips.id, var.default_user_ips]
    description = "Allows port 3306 from our Home IPs"
  }
  ingress {
    cidr_blocks = ["172.31.0.0/16"]
    description = "Allows port 3306 from local VPC"
    from_port = var.mysql_port
    protocol = "tcp"
    to_port = var.mysql_port
  }
  ingress {
    security_groups = [aws_security_group.kubernetes_sg.id]
    description = "Allows port 3306 from kubernetes cluster"
    from_port = var.mysql_port
    protocol = "tcp"
    to_port = var.mysql_port
  }

  egress {
    from_port = var.outbound_port
    to_port = var.outbound_port
    protocol = "-1"
    cidr_blocks = var.open_internet
    ipv6_cidr_blocks = var.ipv6_cidr_block
  }
  tags = {
    "Name" = "${var.team_tag}_Database_SG"
  }
} 

/* ------------------------------ Bitbucket SG ------------------------------ */

resource "aws_security_group" "bajams_bitbucket" {
  name = "bajams_sg_bitbucket"
  tags = {
    "Name" = "${var.team_tag}_Bitbucket_SG"
  }
  vpc_id = var.vpc_id
  description = "Allows Bitbucket Webhook to run"
  ingress {
    cidr_blocks = ["13.52.5.96/28","13.236.8.224/28","18.136.214.96/28","18.184.99.224/28","18.234.32.224/28","18.246.31.224/28","52.215.192.224/28","104.192.137.240/28","104.192.138.240/28","104.192.140.240/28","104.192.142.240/28","104.192.143.240/28","185.166.143.240/28","185.166.142.240/28"]
    description = "Bitbucket IPs:80"
    from_port = 80
    protocol = "tcp"
    to_port = 80
  }
  ingress {
    cidr_blocks = ["172.31.0.0/16"]
    description = "Allows all traffic from local VPC"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }
  ingress {
    cidr_blocks = ["13.52.5.96/28","13.236.8.224/28","18.136.214.96/28","18.184.99.224/28","18.234.32.224/28","18.246.31.224/28","52.215.192.224/28","104.192.137.240/28","104.192.138.240/28","104.192.140.240/28","104.192.142.240/28","104.192.143.240/28","185.166.143.240/28","185.166.142.240/28"]
    description = "Bitbucket IPs:8080"
    from_port = 8080
    protocol = "tcp"
    to_port = 8080
  }
  egress {
    cidr_blocks = var.open_internet
    from_port = var.outbound_port
    ipv6_cidr_blocks = var.ipv6_cidr_block
    protocol = "-1"
    to_port = var.outbound_port
  }
}

/* ------------------------ Kubernetes Security Group ----------------------- */

resource "aws_security_group" "kubernetes_sg" {
  name = "bajams_k8_master_sg"
  vpc_id = var.vpc_id
  description = "Allow access from Jenkins Worker"
  ingress {
    from_port = var.outbound_port
    to_port = var.outbound_port
    protocol = "-1"
    security_groups = [var.sg_jenkins_worker]
  }
  ingress {
    from_port = var.outbound_port
    to_port = var.outbound_port
    protocol = "-1"
    security_groups = [var.sg_jenkins_master]
    description = "Allows all traffic from Jenkins Master"
  }
  ingress {
    from_port = var.outbound_port
    to_port = var.outbound_port
    protocol = "-1"
    cidr_blocks = ["172.31.0.0/16"]
    description = "Allows all traffic from VPC"
  }
  ingress {
    from_port = var.outbound_port
    to_port = var.outbound_port
    protocol = "-1"
    security_groups = [aws_security_group.bajams_sg_home_ips.id, var.default_user_ips]
    description = "Allows all traffic from Bajams Home IPs"
  }
  egress {
    cidr_blocks = var.open_internet
    from_port = var.outbound_port
    ipv6_cidr_blocks = var.ipv6_cidr_block
    protocol = "-1"
    to_port = var.outbound_port
  }
tags = {
    "Environment" = "${var.team_tag}"
    "Name"        = "${var.team_tag}_kubernetes_sg"
    }
}


/* ---------------------------- Jenkins Master SG --------------------------- */
#
# resource "aws_security_group" "bajams_jenkins_master_sg" {
#     name = "bajams_jenkins_master"
#     description = "Allows SSH from BaJaMS Home IPs"
#     tags = {
#       "Name" = "BaJaMS_Jenkins_Master"
#     }
#     vpc_id = var.vpc_id
#    ingress {
#       security_groups = [aws_security_group.bajams_sg_home_ips.id]
#       description = "Allows all traffic from Home IPs"
#       from_port = var.outbound_port
#       protocol = "-1"
#       to_port = var.outbound_port
#     }
# 
#     egress {
#       cidr_blocks = var.open_internet
#       from_port = var.outbound_port
#       ipv6_cidr_blocks = var.ipv6_cidr_block
#       protocol = "-1"
#       to_port = var.outbound_port
#     }
# }

/* -------------------------- # # Jenkins Worker SG ------------------------- */
#
# resource "aws_security_group" "bajams_jenkins_worker_sg" {
#     name = "bajams_jenkins_worker"
#     description = "Allows SSH from Jenkins Master"
#     tags = {
#       "Name" = "BaJaMS_Jenkins_Worker"
#     }
#     vpc_id = var.vpc_id
#    ingress {
#       security_groups = [aws_security_group.bajams_sg_home_ips.id]
#       description = "Allows SSH from Home IPs"
#       from_port = var.ssh_port
#       protocol = "tcp"
#       to_port = var.ssh_port
#     }
#
#     egress {
#       cidr_blocks = var.open_internet
#       from_port = var.outbound_port
#       ipv6_cidr_blocks = var.ipv6_cidr_block
#       protocol = "-1"
#       to_port = var.outbound_port
#     }
# }

# # EKS cluster security group
# resource "aws_security_group" "bajams_sg_eks" {
#     name = "bajams_sg_eks"
#     tags = {
#       "Name" = "BaJaMS_EKS_SG"
#     }
#     vpc_id = var.vpc_id
#
#     ingress {
#       self = true
#       description = "Allows all traffic from EKS Cluster"
#       from_port = var.outbound_port
#       protocol = "-1"
#       to_port = var.outbound_port
#     }
#     egress {
#       cidr_blocks = var.open_internet
#       from_port = var.outbound_port
#       ipv6_cidr_blocks = var.ipv6_cidr_block
#       protocol = "-1"
#       to_port = var.outbound_port
#     }
# }