output "endpoint" {
  value = aws_eks_cluster.bajams_eks_cluster.endpoint
}

output "kubeconfig_certificate_authority_data" {
  value = aws_eks_cluster.bajams_eks_cluster.certificate_authority.0.data
}

output "eks_asg_name" {
 value = aws_eks_node_group.bajams_eks_nodegrp.resources.0.autoscaling_groups.0.name
}