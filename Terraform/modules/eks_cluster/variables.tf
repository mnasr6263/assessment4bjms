variable "region" {}

variable "instance_type" {}

variable "subnets" {
    type = list(string)
}