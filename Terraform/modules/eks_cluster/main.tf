/* ---------------------------- EKS CLUSTER + EKS CLUSTER IAM ROLE  --------------------------- */

resource "aws_eks_cluster" "bajams_eks_cluster" {
  name     = "BaJaMS"
  role_arn = aws_iam_role.bajams_eks_role.arn

  vpc_config {
    endpoint_private_access = false
    endpoint_public_access = true
    public_access_cidrs = ["0.0.0.0/0"]
    subnet_ids = var.subnets
  }
  
  depends_on = [
    aws_iam_role_policy_attachment.a4-AmazonEC2FullAccess,
    aws_iam_role_policy_attachment.a4-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.a4-AmazonEKSServicePolicy,
    aws_iam_role_policy_attachment.a4-AmazonEKSVPCResourceController,
    aws_iam_role_policy_attachment.a4-AmazonEC2FullAccess,
    #aws_iam_role_policy_attachment.a4-AmazonEKSServiceRolePolicy,
  ]
}


resource "aws_iam_role" "bajams_eks_role" {
  name = "BaJaMS-eks-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "a4-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role = aws_iam_role.bajams_eks_role.name
}

resource "aws_iam_role_policy_attachment" "a4-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role = aws_iam_role.bajams_eks_role.name
}

resource "aws_iam_role_policy_attachment" "a4-AmazonEC2FullAccess" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
  role       = aws_iam_role.bajams_eks_role.name
}

resource "aws_iam_role_policy_attachment" "a4-AmazonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.bajams_eks_role.name
}

# resource "aws_iam_role_policy_attachment" "a4-AmazonEKSServiceRolePolicy" {
#   policy_arn = "arn:aws:iam::aws:policy/aws-service-role/AmazonEKSServiceRolePolicy"
#   role       = aws_iam_role.bajams_eks_role.name
# }

/* ---------------------------- EKS NODE GROUP + EKS NODE GROUP IAM ROLE --------------------------- */

resource "aws_eks_node_group" "bajams_eks_nodegrp" {
  cluster_name = aws_eks_cluster.bajams_eks_cluster.name
  node_group_name = "BaJaMS-Node-Group"
  node_role_arn = aws_iam_role.bajams_node_role.arn
  subnet_ids = var.subnets
  instance_types = var.instance_type #e.g. ["t3a.xlarge]

  scaling_config {
    min_size = 1
    max_size = 3
    desired_size = 1
  }
  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.a4-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.a4-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.a4-AmazonEC2ContainerRegistryReadOnly,
  ]
}

resource "aws_iam_role" "bajams_node_role" {
  name = "BaJaMS-node-role"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "a4-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role = aws_iam_role.bajams_node_role.name
}

resource "aws_iam_role_policy_attachment" "a4-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role = aws_iam_role.bajams_node_role.name
}

resource "aws_iam_role_policy_attachment" "a4-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role = aws_iam_role.bajams_node_role.name
}