/* ------------------------ Setting up Jenkins Master ----------------------- */

# resource "aws_instance" "bajams_jenkins_master" {
#   ami = var.jenkins_ami_manager # BaJaMS Jenkins Manager
#   iam_instance_profile = var.iam_instance_profile
#   instance_type = var.instance_type
#   subnet_id = var.jenkins_subnet
#   key_name = var.key_name
#   associate_public_ip_address = true
#   security_groups = [var.jenkins_master_sg, var.default_user_ips, var.bitbucket_sg]
#   vpc_security_group_ids = var.jenkins_master_sg

#   tags = {
#     "Name" = "${var.team_tag}-jenkins-master"
#   }
# }

/* ----------------------- Setting up Jenkins Workers ----------------------- */

# resource "aws_instance" "bajams_jenkins_worker1" {
#   ami = var.jenkins_ami_worker # BaJaMS Jenkins Worker
#   iam_instance_profile = var.iam_instance_profile
#   instance_type = var.instance_type
#   subnet_id = var.jenkins_subnet
#   key_name = var.key_name
#   associate_public_ip_address = true
#   security_groups = [var.jenkins_worker_sg, var.default_user_ips]
#   vpc_security_group_ids = var.jenkins_worker_sg

#   tags = {
#     "Name" = "${var.team_tag}-jenkins-worker1"
#   }
# }

# resource "aws_instance" "bajams_jenkins_worker2" {
#   ami = var.jenkins_ami_worker # BaJaMS Jenkins Worker
#   iam_instance_profile = var.iam_instance_profile
#   instance_type = var.instance_type
#   subnet_id = var.jenkins_subnet
#   key_name = var.key_name
#   associate_public_ip_address = true
#   security_groups = [var.jenkins_worker_sg, var.default_user_ips]
#   vpc_security_group_ids = var.jenkins_worker_sg

#   tags = {
#     "Name" = "${var.team_tag}-jenkins-worker2"
#   }
# }
