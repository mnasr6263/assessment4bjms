# # RDS database
# data "local_file" "RDSpass" {
#   filename = "/home/ec2-user/RDSpass"
# }

/* ------------------------- Setting up RDS database  for Petclinic ------------------------ */

resource "aws_db_instance" "bajams_database_pc" {
  identifier = var.pc_db_name
  allocated_storage = 20
  max_allocated_storage = 100
  multi_az = true
  apply_immediately = true
  storage_type = "gp2"
  engine = "mysql"
  engine_version = "8.0.20"
  instance_class = "db.t2.micro"
  name = "BaJaMS_database_pc"
  username = var.pc_username
  password = var.pc_password #data.local_file.RDSpass.content
  skip_final_snapshot = true
  publicly_accessible = true
  backup_retention_period = 5
  backup_window = "21:00-22:00"
  db_subnet_group_name = "bajams_db_subnet"
  depends_on = [aws_db_subnet_group.bajams_db_subnet]
  vpc_security_group_ids = [var.database_sg, "sg-091c83d3ecee7301e"]
}

/* ------------------ Setting up RDS Database for Wordpress ----------------- */

resource "aws_db_instance" "bajams_database_wp" {
  identifier = var.wp_db_name
  allocated_storage = 20
  max_allocated_storage = 100
  multi_az = true
  apply_immediately = true
  storage_type = "gp2"
  engine = "mysql"
  engine_version = "8.0.20"
  instance_class = "db.t2.micro"
  name = "BaJaMS_database_wp"
  username = var.wp_username
  password = var.wp_password #data.local_file.RDSpass.content
  skip_final_snapshot = true
  publicly_accessible = true
  backup_retention_period = 5
  backup_window = "21:00-22:00"
  db_subnet_group_name = "bajams_db_subnet"
  depends_on = [aws_db_subnet_group.bajams_db_subnet]
  vpc_security_group_ids = [var.database_sg, "sg-091c83d3ecee7301e"]
}

/* ----------------------- Setting up db subnet group ----------------------- */

resource "aws_db_subnet_group" "bajams_db_subnet" {
 name       = "bajams_db_subnet"
 description = "Group of subnets associated with the database subnet group"
 subnet_ids = ["${var.subnetA}", "${var.subnetB}", "${var.subnetC}"]

 tags = {
   Name = "BaJaMS DB subnet"
 }
}

# # ----------- write db host name to local file ----------
# resource "local_file" "dbhost" {
#     content     = aws_db_instance.bajams_database.address
#     filename = "/home/ec2-user/dbhost"
# }
# # ------------ write username to local file -------------
# resource "local_file" "dbuser" {
#     content     = aws_db_instance.bajams_database.username
#     filename = "/home/ec2-user/dbuser"
# }
# # ------------ write password to local file ---------
# resource "local_file" "dbpass" {
#     content     = aws_db_instance.bajams_database.password
#     filename = "/home/ec2-user/dbpass"
# }

/* -------------- Setting up RDS replica Database for Petclinic ------------- */
# and setting up a route DNS for this database

resource "aws_db_instance" "pc_db_replica" {
  allocated_storage = 20
  storage_type = "gp2"
  multi_az = true
  engine = "mysql"
  engine_version = "8.0.20"
  instance_class = "db.t2.micro"
  skip_final_snapshot = true
  vpc_security_group_ids = [var.database_sg]
  identifier = "${var.pc_env_tag}-db-replica"

  replicate_source_db = "${aws_db_instance.bajams_database_pc.identifier}"
}

resource "aws_route53_record" "pc_database" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "bajams-db-pc.academy.labs.automationlogic.com"
  type = "CNAME"
  ttl = "300"
  records = [aws_db_instance.bajams_database_pc.address]
}

/* -------------- Setting up RDS replica Database for Wordpress ------------- */
# and setting up a route DNS for this database

resource "aws_db_instance" "wp_db_replica" {
  allocated_storage = 20
  storage_type = "gp2"
  multi_az = true
  engine = "mysql"
  engine_version = "8.0.20"
  instance_class = "db.t2.micro"
  skip_final_snapshot = true
  vpc_security_group_ids = [var.database_sg]
  identifier = "${var.wp_env_tag}-db-replica"

  replicate_source_db = "${aws_db_instance.bajams_database_wp.identifier}"
}

resource "aws_route53_record" "wp_database" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "bajams-db-wp.academy.labs.automationlogic.com"
  type = "CNAME"
  ttl = "300"
  records = [aws_db_instance.bajams_database_wp.address]
}
