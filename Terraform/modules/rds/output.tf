output "pc_rds_endpoint" {
    value = aws_db_instance.bajams_database_pc.address
}

output "pc_rds_username" {
    value = aws_db_instance.bajams_database_pc.username
}

output "pc_rds_password" {
    value = aws_db_instance.bajams_database_pc.password
    sensitive   = true
}

output "wp_rds_endpoint" {
    value = aws_db_instance.bajams_database_wp.address
}

output "wp_rds_username" {
    value = aws_db_instance.bajams_database_wp.username
}

output "wp_rds_password" {
    value = aws_db_instance.bajams_database_wp.password
    sensitive   = true
}

output "pc_rds_arn" {
    value = aws_db_instance.bajams_database_pc.arn
}

output "wp_rds_arn" {
    value = aws_db_instance.bajams_database_wp.arn
}