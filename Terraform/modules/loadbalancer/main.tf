/* ------------------------------- Loadbalancer ------------------------------- */

resource "aws_lb" "bajams_aws_lb" {
    name = "Bajams-loadbalancer"
    internal = false
    load_balancer_type = "application"
    subnets = var.subnet_ids
    security_groups = var.bajams_lb_sg
    enable_deletion_protection = false

    tags = {
        Environment = "production"
    }
}

/* ------------------------------- Target Group ------------------------------- */

resource "aws_lb_target_group" "bajams_lb_target_group" {
  name     = "Bajams-lb-tg"
  port     = 30006
  protocol = "HTTP"
  vpc_id   = var.vpc_id #### aws_vpc id
}

resource "aws_lb_target_group_attachment" "bajams_master_node" {
  target_group_arn = aws_lb_target_group.bajams_lb_target_group.arn
  target_id        = var.master0_ip
  port             = 30006
}
/* -------------------------- Loadbalancer listener -------------------------- */

resource "aws_lb_listener" "bajams_front_end" {
  load_balancer_arn = aws_lb.bajams_aws_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.bajams_lb_target_group.arn
  }
}

/* -------------------------------- Route 53 -------------------------------- */

resource "aws_route53_record" "petclinic_www" {
  zone_id = "Z03386713MCJ0LHJCA6DL" # AL academy 
  name    = "bajams-petclinic"
  type    = "A"
  alias {
    name                   = aws_lb.bajams_aws_lb.dns_name
    zone_id                = aws_lb.bajams_aws_lb.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "wordpress_www" {
  zone_id = "Z03386713MCJ0LHJCA6DL" # AL academy 
  name    = "bajams-wordpress"
  type    = "A"
  alias {
    name                   = aws_lb.bajams_aws_lb.dns_name
    zone_id                = aws_lb.bajams_aws_lb.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "static_www" {
  zone_id = "Z03386713MCJ0LHJCA6DL" # AL academy 
  name    = "bajams-static"
  type    = "A"
  alias {
    name                   = aws_lb.bajams_aws_lb.dns_name
    zone_id                = aws_lb.bajams_aws_lb.zone_id
    evaluate_target_health = true
  }
}
