variable "subnet_ids" {
    type = list(string)
}

variable "bajams_lb_sg" {
    type = list(string)
}

variable "vpc_id" {}

variable "region" {}

variable "master0_ip" {
    type = string
}