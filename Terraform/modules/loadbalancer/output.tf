output "route53_pc" {
    value = "${aws_route53_record.petclinic_www.name}.academy.labs.automationlogic.com"
}

output "route53_wp" {
    value = "${aws_route53_record.wordpress_www.name}.academy.labs.automationlogic.com"
}

output "route53_static" {
    value = "${aws_route53_record.static_www.name}.academy.labs.automationlogic.com"
}