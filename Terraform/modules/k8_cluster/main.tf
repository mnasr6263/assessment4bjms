/* ------------------ Kubernetes Master and Nodes Instances ----------------- */

resource "aws_instance" "k8_bastion" {
  ami = var.k8_cluster_ami
  root_block_device {
        volume_size = 20
    }
  instance_type = var.instance_type
  availability_zone = "eu-west-1c"
  associate_public_ip_address = true
  vpc_security_group_ids = [var.kubernetes_sg, "sg-091c83d3ecee7301e"]
  iam_instance_profile = var.iam_instance_role
  key_name = var.ssh_key
  
  user_data = base64encode(templatefile(".//modules/k8_cluster/provision.tpl", 
  {
    priv_ip_master1 = "${aws_instance.k8_master[0].private_ip}"
    priv_ip_master2 = "${aws_instance.k8_master[1].private_ip}"
    priv_ip_nodeA = "${aws_instance.k8_nodeA.private_ip}"
    priv_ip_nodeB = "${aws_instance.k8_nodeB.private_ip}"
    priv_ip_nodeC = "${aws_instance.k8_nodeC.private_ip}"
    s3_bucket_id = "${var.s3_bucket_id}"

  }
  ))
  tags = {
      "Name" = "${var.team_tag}-k8-Bastion-Controller"
  }
}

resource "aws_instance" "k8_master" {
  ami = var.k8_cluster_ami
  root_block_device {
        volume_size = 30
    }
  instance_type = var.instance_k8_type
  availability_zone = "eu-west-1c"
  associate_public_ip_address = true
  vpc_security_group_ids = [var.kubernetes_sg, var.bajams_sg_home_ips]
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  key_name = var.ssh_key
  count = 2
  user_data = "${base64encode(<<-EOF
            #!/bin/bash
            sudo yum update -y

            cat ${var.k8_pub_key} >> /home/ec2-user/.ssh/authorized_keys
            echo ${var.dbhost_pc} > /home/ec2-user/dbinfo_pc
            echo ${var.dbuser_pc} >> /home/ec2-user/dbinfo_pc
            echo ${var.dbpass_pc} >> /home/ec2-user/dbinfo_pc
            echo ${var.dbhost_wp} > /home/ec2-user/dbinfo_wp
            echo ${var.dbuser_wp} >> /home/ec2-user/dbinfo_wp
            echo ${var.dbpass_wp} >> /home/ec2-user/dbinfo_wp
            echo ${var.route53_pc} > /home/ec2-user/route53_pc
            echo ${var.route53_wp} > /home/ec2-user/route53_wp
            echo ${var.route53_static} > /home/ec2-user/route53_static

            #Install git
            sudo yum install git -y

            # Clone HAProxy repo
            git clone https://github.com/haproxytech/kubernetes-ingress.git /home/ec2-user/kubernetes-ingress

            EOF
  )}"
  tags = {
      "Name" = "${var.team_tag}-k8-Master-${count.index}"
  }
}

resource "aws_instance" "k8_nodeA" {
  ami = var.k8_cluster_ami
  root_block_device {
        volume_size = 30
    }
  instance_type = "t3a.xlarge"
  availability_zone = "eu-west-1c"
  associate_public_ip_address = true
  vpc_security_group_ids = [var.kubernetes_sg]
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  key_name = var.ssh_key
  user_data = "${base64encode(<<-EOF
            #!/bin/bash
            sudo yum update -y

            cat ${var.k8_pub_key} >> /home/ec2-user/.ssh/authorized_keys

            #Install git
            sudo yum install git -y
            
            
            EOF
  )}"
  tags = {
      "Name" = "${var.team_tag}-k8-NodeA"
  }
}

resource "aws_instance" "k8_nodeB" {
  ami = var.k8_cluster_ami
  root_block_device {
        volume_size = 30
    }
  instance_type = "t3a.xlarge"
  availability_zone = "eu-west-1c"
  associate_public_ip_address = true
  vpc_security_group_ids = [var.kubernetes_sg]
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  key_name = var.ssh_key
  user_data = "${base64encode(<<-EOF
            #!/bin/bash
            sudo yum update -y

            cat ${var.k8_pub_key} >> /home/ec2-user/.ssh/authorized_keys

            #Install git
            sudo yum install git -y
            
            
            EOF
  )}"
  tags = {
      "Name" = "${var.team_tag}-k8-NodeB"
  }
}

resource "aws_instance" "k8_nodeC" {
  ami = var.k8_cluster_ami
  root_block_device {
        volume_size = 30
    }
  instance_type = "t3a.xlarge"
  availability_zone = "eu-west-1c"
  associate_public_ip_address = true
  vpc_security_group_ids = [var.kubernetes_sg]
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  key_name = var.ssh_key
  user_data = "${base64encode(<<-EOF
            #!/bin/bash
            sudo yum update -y

            cat ${var.k8_pub_key} >> /home/ec2-user/.ssh/authorized_keys

            #Install git
            sudo yum install git -y
            
            
            EOF
  )}"
  tags = {
      "Name" = "${var.team_tag}-k8-NodeC"
  }
}
/* ------------------- Create Local Files with Private IPs ------------------ */

resource "local_file" "controller" {
    content = aws_instance.k8_bastion.private_ip
    filename = "/home/ec2-user/bastion_ip"
}
resource "local_file" "master0" {
    content = aws_instance.k8_master[0].private_ip
    filename = "/home/ec2-user/master0_ip"
}

resource "local_file" "master1" {
    content = aws_instance.k8_master[1].private_ip
    filename = "/home/ec2-user/master1_ip"
}

resource "local_file" "node1" {
    content = aws_instance.k8_nodeA.private_ip
    filename = "/home/ec2-user/nodeA_ip"
}

resource "local_file" "node2" {
    content = aws_instance.k8_nodeB.private_ip
    filename = "/home/ec2-user/nodeB_ip"
}
resource "local_file" "node3" {
    content = aws_instance.k8_nodeC.private_ip
    filename = "/home/ec2-user/nodeC_ip"
}

