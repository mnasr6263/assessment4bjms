#!/bin/bash
sudo yum update -y
aws s3 cp s3://${s3_bucket_id}/BajamsKey.pem /home/ec2-user/.ssh/BajamsKey.pem
sudo chown ec2-user:ec2-user /home/ec2-user/.ssh/BajamsKey.pem
sudo chmod 400 /home/ec2-user/.ssh/BajamsKey.pem

#Generate SSH key for access to nodes
ssh-keygen -b 2048 -t rsa -f /home/ec2-user/.ssh/sshkey -q -N ""

#Allow ec2-user access
sudo chown ec2-user:ec2-user /home/ec2-user/.ssh/sshkey
sudo chown ec2-user:ec2-user /home/ec2-user/.ssh/sshkey.pub
sudo chmod 400 /home/ec2-user/.ssh/sshkey

#Copy the public ssh key and bajamske.pem to s3 bucket
aws s3 cp /home/ec2-user/.ssh/sshkey.pub s3://${s3_bucket_id}/sshkey.pub

#Install Kubespray requirements
sudo yum install -y git
yes | git clone https://github.com/kubernetes-sigs/kubespray.git /home/ec2-user/kubespray
sudo pip3 install -r /home/ec2-user/kubespray/requirements.txt
cp -rfp /home/ec2-user/kubespray/inventory/sample /home/ec2-user/kubespray/inventory/mycluster


#Edit hosts file
echo "all:
  hosts:
    node1:
      ansible_host: ${priv_ip_master1}
      ip: ${priv_ip_master1}
      access_ip: ${priv_ip_master1}
    node2:
      ansible_host: ${priv_ip_master2}
      ip: ${priv_ip_master2}
      access_ip: ${priv_ip_master2}
    node3:
      ansible_host: ${priv_ip_nodeA}
      ip: ${priv_ip_nodeA}
      access_ip: ${priv_ip_nodeA}
    node4:
      ansible_host: ${priv_ip_nodeB}
      ip: ${priv_ip_nodeB}
      access_ip: ${priv_ip_nodeB}
    node5:
      ansible_host: ${priv_ip_nodeC}
      ip: ${priv_ip_nodeC}
      access_ip: ${priv_ip_nodeC}
  children:
    kube_control_plane:
      hosts:
        node1:
        node2:
    kube_master:
      hosts:
        node1:
        node2:
    kube_node:
      hosts:
        node3:
        node4:
        node5:
    etcd:
      hosts:
        node1:
        node2:
        node3:
        node4:
        node5:
    k8s_cluster:
      children:
        kube_control_plane:
        kube_master:
        kube_node:
    calico_rr:
      hosts: {}
">/home/ec2-user/kubespray/inventory/mycluster/hosts.yaml

#Edit ansible config file for path access to ssh key
sudo sh -c "cat >/home/ec2-user/kubespray/ansible.cfg <<_END_
[ssh_connection]
pipelining=True
ssh_args = -o ControlMaster=auto -o ControlPersist=30m -o ConnectionAttempts=100 -o UserKnownHostsFile=/dev/null
#control_path = ~/.ssh/ansible-%%r@%%h:%%p
[defaults]
strategy_plugins = plugins/mitogen/ansible_mitogen/plugins/strategy
# https://github.com/ansible/ansible/issues/56930 (to ignore group names with - and .)
force_valid_group_names = ignore
private_key_file = /home/ec2-user/.ssh/sshkey

host_key_checking = False
gathering = smart
fact_caching = jsonfile
fact_caching_connection = /tmp
fact_caching_timeout = 7200
stdout_callback = default
display_skipped_hosts = no
library = ./library
callback_whitelist = profile_tasks
roles_path = roles:$VIRTUAL_ENV/usr/local/share/kubespray/roles:$VIRTUAL_ENV/usr/local/share/ansible/roles:/usr/share/kubespray/roles
deprecation_warnings=False
inventory_ignore_extensions = ~, .orig, .bak, .ini, .cfg, .retry, .pyc, .pyo, .creds, .gpg
[inventory]
ignore_patterns = artifacts, credentials
_END_"

#Allow ec2-user access
sudo chown -R ec2-user:ec2-user /home/ec2-user/*

# # Installation of Kubernetes via kubespray ansible playbook

# cd /home/ec2-user/kubespray
# export AWS_DEFAULT_PROFILE=Academy
# ansible-playbook -i /home/ec2-user/kubespray/inventory/mycluster/hosts.yaml --become --become-user=root /home/ec2-user/kubespray/cluster.yml
