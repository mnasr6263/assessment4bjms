#!/bin/bash
keypair=sshkey.pub
s3_bucket_id=$(cat /home/ec2-user/s3_bucket_id)
aws s3 cp s3://$s3_bucket_id/$keypair /home/ec2-user/.ssh/$keypair
bastion=$(cat /home/ec2-user/bastion_ip)

priv_ip_master1=$(cat /home/ec2-user/master0_ip)
priv_ip_master2=$(cat /home/ec2-user/master1_ip)
priv_ip_nodeA=$(cat /home/ec2-user/nodeA_ip)
priv_ip_nodeB=$(cat /home/ec2-user/nodeB_ip)
priv_ip_nodeC=$(cat /home/ec2-user/nodeC_ip)

ssh -o StrictHostKeyChecking=no -i ~/.ssh/sshkey ec2-user@$priv_ip_master1 '
aws s3 cp s3://$s3_bucket_id/sshkey.pub /home/ec2-user/.ssh/sshkey.pub 
cat /home/ec2-user/.ssh/sshkey.pub >> /home/ec2-user/.ssh/authorized_keys
'
ssh -o StrictHostKeyChecking=no -i ~/.ssh/sshkey ec2-user@$priv_ip_master2 '
aws s3 cp s3://$s3_bucket_id/sshkey.pub /home/ec2-user/.ssh/sshkey.pub 
cat /home/ec2-user/.ssh/sshkey.pub >> /home/ec2-user/.ssh/authorized_keys
'
ssh -o StrictHostKeyChecking=no -i ~/.ssh/sshkey ec2-user@$priv_ip_nodeA '
aws s3 cp s3://$s3_bucket_id/sshkey.pub /home/ec2-user/.ssh/sshkey.pub 
cat /home/ec2-user/.ssh/sshkey.pub >> /home/ec2-user/.ssh/authorized_keys 
'

ssh -o StrictHostKeyChecking=no -i ~/.ssh/sshkey ec2-user@$priv_ip_nodeB '
aws s3 cp s3://$s3_bucket_id/sshkey.pub /home/ec2-user/.ssh/sshkey.pub 
cat /home/ec2-user/.ssh/sshkey.pub >> /home/ec2-user/.ssh/authorized_keys 
'
ssh -o StrictHostKeyChecking=no -i ~/.ssh/sshkey ec2-user@$priv_ip_nodeC'
aws s3 cp s3://$s3_bucket_id/sshkey.pub /home/ec2-user/.ssh/sshkey.pub 
cat /home/ec2-user/.ssh/sshkey.pub >> /home/ec2-user/.ssh/authorized_keys 
'

ssh -o StrictHostKeyChecking=no -i ~/.ssh/BajamsKey.pem ec2-user@$bastion_ip '
cd /home/ec2-user/kubespray
export AWS_DEFAULT_PROFILE=Academy
ansible-playbook -i /home/ec2-user/kubespray/inventory/mycluster/hosts.yaml --become --become-user=root /home/ec2-user/kubespray/cluster.yml
'
