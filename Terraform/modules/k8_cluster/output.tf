output "k8_master0_id" {
    value = aws_instance.k8_master[0].id
}

output "k8_master1_id" {
    value = aws_instance.k8_master[1].id
}
