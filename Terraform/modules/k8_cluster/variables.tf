variable "region" {}

variable "vpc_id" {}

variable "k8_cluster_ami" {
    type = string
    description = "ami for kubernetes cluster instances"
}

variable "kubernetes_sg" {
    type= string
}

variable "bajams_sg_home_ips" {
    type= string
}

variable "team_tag" {
    type = string
    description = "team name, BaJaMS"
}

variable "iam_instance_role" {
    type = string
}

variable "instance_type" {
    type = string
}

variable "instance_k8_type" {
    type = string
}

variable "ssh_key" {
    type = string
}
variable "sg_home_ips" {
    type = string
}

variable "s3_bucket_id" {
    type = string
}

variable "k8_pub_key" {
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCLV87aZoRXfRITMPYp14PI/wN4rAhAtQpAo5S1mTsekfrbPFZb3h8ISBetPJShV0JngHV8Z6IAqYPDmytgCUl218yYKICFRbA8dj4z4NaOvnSdAvIcxySsoy9ueXzvC7Kte2/qNYa3OvdnweLmAWXCIGy/bW9q9OGVjcOi1HbjRloNZ7EeassEZtrLxRm6LC9fgFMoaZaZvguOj3zdXfF2on+qcz2b1DOR8irRBNuYdFJpzW4eXNZnQZLydglbfM36FL36yzs30lVBtymF3ew9ksQnfzuVQyYegdmWxBOu/Jp0NC2xFGpMDb1L42FM8vdJeYBmgdto9SO9zLRithcj BajamsKey"
}

variable "dbhost_pc" {
    type = string
}

variable "dbpass_pc" {
    type = string
}

variable "dbuser_pc" {
    type = string
}

variable "dbhost_wp" {
    type = string
}

variable "dbpass_wp" {
    type = string
}

variable "dbuser_wp" {
    type = string
}

variable "route53_pc" {
    type = string
}

variable "route53_wp" {
    type = string
}

variable "route53_static" {
    type = string
}