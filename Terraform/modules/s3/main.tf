/* -------------------------------------------------------------------------- */
/*                                  S3 Bucket                                 */
/* -------------------------------------------------------------------------- */

resource "aws_s3_bucket" "default" {
  bucket = "bajams-bucket"
  acl    = "private"
  force_destroy = true
  versioning {
  enabled = true
  }

  tags = {
    Name = "BaJaMS-bucket"
  }
}

/* ----------------------------- Bajams Key pem ----------------------------- */

resource "aws_s3_bucket_object" "object" {
  bucket = aws_s3_bucket.default.id
  key    = "BajamsKey.pem"
  acl    = "private"  # or can be "public-read"
  source = "/home/ec2-user/.ssh/BajamsKey.pem"
}

resource "local_file" "s3_bucket" {
    content = aws_s3_bucket.default.id
    filename = "/home/ec2-user/s3_bucket_id"
}