output "s3_bucket_id" {
    value = aws_s3_bucket.default.id
    description = "id of s3 bucket"
}