/* ---------------------- create/modify security groups --------------------- */
# bitbucket (webhook), home ips (dev ips), bastion, master,
# database (db), loadbalancer, webservers.

module "security_groups" {
    source = ".//modules/security_groups"
    vpc_id = var.vpc_id
    region = var.region
    sg_jenkins_master = "sg-06e0183903739bc65"
    sg_jenkins_worker = "sg-09fec6a0109cfca56"
    default_user_ips = "sg-091c83d3ecee7301e"
    team_tag = var.team_name_tag
}

/* ---------------------------- create/modify RDS --------------------------- */
#  db instance, db subnet group, localfiles: dbhost, dbpass, dbuser [rds endpoint info]

module "rds_database" {
    source = ".//modules/rds"
    pc_username = var.pc_rds_username
    pc_password = var.pc_rds_password
    wp_username = var.wp_rds_username
    wp_password = var.wp_rds_password
    pc_db_name = var.pc_database_name
    wp_db_name = var.wp_database_name
    subnetA = var.subnet_1c
    subnetB = var.subnet_1b
    subnetC = var.subnet_1a
    database_sg = module.security_groups.database_sg
    pc_env_tag = var.pc_env_tag
    wp_env_tag = var.wp_env_tag
    region = var.region    
}

/* ---------------------------- create/modify EKS cluster --------------------------- */
# AWS EKS cluster manager and node setup, iam roles and cluster SG and intergrated auto scaling

# module "bajams_cluster" {
#     source = ".//modules/eks_cluster"
#     subnets = ["${var.subnet_1a}", "${var.subnet_1b}", "${var.subnet_1c}"]
#     instance_type = ["${var.instance_type_input}"]
#     region = var.region
# }

# resource "aws_autoscaling_policy" "eks-scale" {
#  name                   = "eks-scale"
#  policy_type            = "TargetTrackingScaling"
#  autoscaling_group_name = module.bajams_cluster.eks_asg_name

#  target_tracking_configuration {
#    predefined_metric_specification {
#      predefined_metric_type = "ASGAverageCPUUtilization"
#    }

#  target_value = 80.0
#  }
# }

/* ---------------------------- create/modify Kubernetes cluster --------------------------- */
# kubernetes controller, master and node instances, kuberneters master sg 

module "k8_clusters" {
    source = ".//modules/k8_cluster"
    k8_cluster_ami = "ami-063d4ab14480ac177"
    iam_instance_role = "ALAcademyJenkinsSuperRole" 
    vpc_id = var.vpc_id
    team_tag = var.team_name_tag
    ssh_key = var.key_name
    instance_type = var.instance_type
    instance_k8_type = var.instance_type_input
    kubernetes_sg = module.security_groups.kubernetes_sg
    bajams_sg_home_ips = module.security_groups.home_ips_sg
    sg_home_ips = module.security_groups.home_ips_sg
    region = var.region
    s3_bucket_id = module.s3.s3_bucket_id
    dbhost_pc = module.rds_database.pc_rds_endpoint
    dbuser_pc = module.rds_database.pc_rds_username
    dbpass_pc = module.rds_database.pc_rds_password
    dbhost_wp = module.rds_database.wp_rds_endpoint
    dbpass_wp = module.rds_database.wp_rds_password
    dbuser_wp = module.rds_database.wp_rds_username
    route53_pc = module.loadbalancer.route53_pc
    route53_wp = module.loadbalancer.route53_wp
    route53_static = module.loadbalancer.route53_static
}

/* ---------------------------- create/modify s3 bucket --------------------------- */
# s3 bucket and key pem object

module "s3" {
    source = ".//modules/s3"
    team_name_tag = var.team_name_tag
    region = var.region
}

/* ---------------------------- create/modify loadbalancer --------------------------- */
# loadbalancer for k8 master node

module "loadbalancer" {
    source = ".//modules/loadbalancer"
    vpc_id = var.vpc_id
    subnet_ids = ["${var.subnet_1a}", "${var.subnet_1b}", "${var.subnet_1c}"]
    bajams_lb_sg = [module.security_groups.loadbalancer_sg]
    region = var.region
    master0_ip = module.k8_clusters.k8_master0_id
}

/* --------------------- create/modify jenkins instances -------------------- */
# Jenkins master, jenkins worker 1 and worker 2

# module "jenkins" {
#     source = ".//modules/jenkins"
#     vpc_id = var.vpc_id
#     jenkins_ami_manager = "ami-0248a862b7759b701"
#     jenkins_ami_worker = "ami-08c5b82e8a2ccccbc"
#     iam_instance_profile = "ALAcademyJenkinsSuperRole"
#     instance_type = var.instance_type
#     jenkins_subnet = var.subnet_1c
#     key_name = var.key_name
#     jenkins_master_sg = "sg-06e0183903739bc65"
#     jenkins_worker_sg = "sg-09fec6a0109cfca56"
#     bitbucket_sg = module.security_groups.bitbucket_sg
#     default_user_ips = "sg-091c83d3ecee7301e"
#     team_tag = var.team_name_tag
#     region = var.region
# }