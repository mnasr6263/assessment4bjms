Namespaceexitcode=0
Name=wordpress
i=0
# loop through wordpress namespaces and delete the route53 record associated with namespace
while [[ $Namespaceexitcode == "0" ]]; do
   newName=$Name$i
   kubectl get namespace | grep "$newName"
   Namespaceexitcode=$?
   if [[ $Namespaceexitcode == "0" ]]; then
   echo '{
                "Comment": "DELETE a record ",
                "Changes": [{
                "Action": "DELETE",
                            "ResourceRecordSet": {
                                        "Name": "bajams-'$newName'-wp.academy.labs.automationlogic.com",
                                        "Type": "A",
                                        "AliasTarget": {"HostedZoneId":"Z32O12XQLNTSW2","DNSName": "Bajams-loadbalancer-329416162.eu-west-1.elb.amazonaws.com", "EvaluateTargetHealth": true}
    }}]
    }' > $newName.json
    aws route53 change-resource-record-sets --hosted-zone-id Z03386713MCJ0LHJCA6DL --change-batch file:///home/ec2-user/assessment4bjms/destroy/$newName.json
    kubectl delete namespace $newName
    fi
    let i++
done