#!/bin/bash
# Assumes kubectl is already on machine
# ------------------------------ install kompose ----------------------------- #
# Linux
# curl -L https://github.com/kubernetes/kompose/releases/download/v1.22.0/kompose-linux-amd64 -o kompose
# sudo chmod +x kompose
# sudo mv kompose /usr/local/bin/kompose

# ----------------------- pull from private docker repo ---------------------- #
sudo yum -y install docker
sudo systemctl start docker
sudo chmod 666 /var/run/docker.sock
aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 242392447408.dkr.ecr.eu-west-1.amazonaws.com
docker pull 242392447408.dkr.ecr.eu-west-1.amazonaws.com/bajams-petclinic:latest

# --------- create docker-compose.yml from template file with db info -------- #
## vm needs variables
DBHOST=$(sed -n 1p /home/ec2-user/dbinfo_pc)
DBUSER=$(sed -n 2p /home/ec2-user/dbinfo_pc)
DBPASS=$(sed -n 3p /home/ec2-user/dbinfo_pc)
DBNAME=petclinic
sudo sed -e "s/%DBHOST%/$DBHOST/" \
    -e "s/%DBUSER%/$DBUSER/" \
    -e "s/%DBPASS%/$DBPASS/" -e "s/%DBTAB%/$DBNAME/" docker-composer.tpl > docker-compose.yml

# -- generate petclinic deployment,service yaml files using kompose convert -- #
# sudo chown ec2-user:ec2-user /home/ec2-user/assessment4bjms/petclinic_komposer/
kompose convert
sudo sed -i "s/  replicas: 1/  replicas: 3/g" petclinic-deployment.yaml
sed -i "s/resources: {}/resources:\n            requests:\n              cpu: \"200m\"\n              memory: \"64Mi\"/g" petclinic-deployment.yaml

# --------------------- create ingress rule from template -------------------- #
#find service name within service.yaml file
SVCNAME=$(grep '^  name:' $(find . -name "*service*") | cut -d ':' -f2 | cut -d ' ' -f2)
# define variables needed for templating
METANAME=$SVCNAME-ingress # CHANGE INGRESS NAME HERE
ROUTE53HOST=$(cat /home/ec2-user/route53_pc) # VARIABLE SUPPLIED VIA TERRAFORM
# Templating
sudo sed -e "s/ROUTE53HOST/$ROUTE53HOST/" \
    -e "s/SVCNAME/$SVCNAME/" \
    -e "s/METANAME/$METANAME/" petclinic-ingress.tpl > petclinic-ingress.yaml
    
# create namespace
sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf
kubectl create namespace petclinic
# apply deployment, service and ingress of petclinic
kubectl apply -f petclinic-deployment.yaml,petclinic-service.yaml,petclinic-ingress.yaml -n petclinic
kubectl autoscale deployment petclinic --cpu-percent=80 --min=1 --max=10 -n petclinic
git add .
git commit -m "creation of deployment,service of petclinic"