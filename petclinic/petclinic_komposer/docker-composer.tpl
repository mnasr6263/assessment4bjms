version: "3.7"
services:
  petclinic:
    build:
      context: .
      dockerfile: Dockerfile
    image: 242392447408.dkr.ecr.eu-west-1.amazonaws.com/bajams-petclinic:latest
    container_name: petclinic
    ports:
      - 80:8080
    environment:
      - DBSERVERNAME=%DBHOST%
      - DBUSERNAME=%DBUSER%
      - DBPASSWORD=%DBPASS%
      - DBNAME=%DBTAB%