apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: wordpress
  name: wordpress-deployment
  labels:
    app: wordpress
spec:
  selector:
    matchLabels:
      app: wordpress
  template:
    metadata:
      labels:
        app: wordpress
    spec:
      containers:
      - image: wordpress
        name: wordpress
        env:
        - name: WORDPRESS_DB_HOST
          value: DBHOST
        - name: WORDPRESS_DB_USER
          value: DBUSER
        - name: WORDPRESS_DB_PASSWORD
          value: DBPASS
        - name: WORDPRESS_DB_NAME
          value: DBNAME
        ports:
        - containerPort: 80
        resources:
          requests:
            cpu: "200m"
            memory: "64Mi"