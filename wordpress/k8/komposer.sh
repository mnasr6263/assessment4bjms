# create wordpress namespace
wpns=wordpress
kubectl create ns $wpns
# get db info from file
WPDBHOST=$(sed -n 1p /home/ec2-user/dbinfo_wp)
WPDBUSER=$(sed -n 2p /home/ec2-user/dbinfo_wp)
WPDBPASS=$(sed -n 3p /home/ec2-user/dbinfo_wp)
WPDBNAME=$wpns

# create schema in DB
sudo yum update
sudo yum install mariadb-server -y
mysql -h $WPDBHOST -u $WPDBUSER -p$WPDBPASS -e "CREATE DATABASE IF NOT EXISTS $WPDBNAME;"

# generate deployment file from template with dbinfo and kubectl apply
sed -e "s/DBHOST/$WPDBHOST/g" -e "s/DBUSER/$WPDBUSER/g" -e "s/DBPASS/$WPDBPASS/g" -e "s/DBNAME/$WPDBNAME/g" wp-deploy.tpl > wp-deploy.yaml
kubectl apply -f wp-deploy.yaml,wp-service.yaml,wp-ingress.yaml
kubectl autoscale deployment wordpress-deployment --cpu-percent=50 --min=1 --max=10 -n $wpns