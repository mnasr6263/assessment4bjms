version: "3.7"
services:
  static:
    build:
      context: .
      dockerfile: Dockerfile
    image: 242392447408.dkr.ecr.eu-west-1.amazonaws.com/bajams-static:latest
    container_name: static
    ports:
      - 80:80