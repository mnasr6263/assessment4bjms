apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: METANAME
spec:
  rules:
    - host: "ROUTE53HOST"
      http:
        paths:
        - path: /
          pathType: Prefix
          backend:
            service:
              name: SVCNAME
              port:
                number: 80
    - http:
        paths:
        - path: /
          pathType: Prefix
          backend:
            service:
              name: SVCNAME
              port:
                number: 80
