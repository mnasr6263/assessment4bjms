<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>BaJaMS</title>
        <link rel="shortcut icon" href="./image/taroinsta.png"/>
        <!-- <link rel="shortcut icon" href="https://toppng.com/uploads/preview/head-dog-11551049702wa6pl7bbjd.png"/> -->
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <style>
            .center_img {
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 50%;
                }
    
            .center_txt {
                text-align: center;
                font-weight: bold;
            }
    
            .box {
            float: left;
            width: 33.33%;
            padding: 50px;
            }
            .clearfix::after {
            content: "";
            clear: both;
            display: table;
            }
        </style>
    </head>
<body>
    <h6 style="text-align:right">BaJaMS</h6>
    <iframe width="100%" height="700" src="https://www.youtube.com/embed/dQw4w9WgXcQ?autoplay=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <br>
    <div class="w3-container w3-blue">
        <h2></h2>
    </div>
    <div class="w3-container w3-light-blue">
        <a href="index.php"><h4 class="center_txt">RETURN TO TARO</h4></a>
    </div>
</body>
</html>