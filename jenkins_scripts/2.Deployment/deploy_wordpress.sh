#!/bin/bash 
keypair=BajamsKey.pem
bastion_ip=$(cat /home/ec2-user/bastion_ip)
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$bastion_ip '

## ssh into k8-master
keypair=BajamsKey.pem
priv_ip_master1=$(cat /home/ec2-user/master0_ip)
ssh -i ~/.ssh/$keypair ec2-user@$priv_ip_master1 "

sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf

# create wordpress namespace 
kubectl create ns wordpress 

# cd into repo 
cd assessment4bjms/wordpress/k8
kubectl apply -f wp-deploy.yaml,wp-service.yaml,wp-ingress.yaml

## run haproxy yaml file 
kubectl apply -f /home/ec2-user/kubernetes-ingress/deploy/haproxy.yaml -n haproxy-controller
"
'