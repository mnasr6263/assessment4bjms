pipeline {
    agent {label 'worker1'}

    stages {
        stage('Git clone in k8 master') {
            steps {
            sh '''keypair=BajamsKey.pem
bastion_ip=$(cat /home/ec2-user/bastion_ip)
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$bastion_ip \'
keypair=BajamsKey.pem
priv_ip_master1=$(cat /home/ec2-user/master0_ip)
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$priv_ip_master1 /bin/bash << _EOF
sudo rm -rf assessment4bjms
git clone https://bitbucket.org/mnasr6263/assessment4bjms.git /home/ec2-user/assessment4bjms
sudo chown -R ec2-user:ec2-user /home/ec2-user/*
_EOF
\''''    
            }
        }
        stage('Pin specific port to haproxy ingress') {
            steps {
            sh '''keypair=BajamsKey.pem
bastion_ip=$(cat /home/ec2-user/bastion_ip)
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$bastion_ip \'
keypair=BajamsKey.pem
priv_ip_master1=$(cat /home/ec2-user/master0_ip)
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$priv_ip_master1 /bin/bash << _EOF
sudo sed -e "213 s/    targetPort: 80/    targetPort: 80\\n    nodePort: 30006/" /home/ec2-user/kubernetes-ingress/deploy/haproxy-ingress.yaml > /home/ec2-user/kubernetes-ingress/deploy/haproxy.yaml
_EOF
\''''    
            }
        }
        stage('Install Kompose convert') {
            steps {
            sh '''keypair=BajamsKey.pem
bastion_ip=$(cat /home/ec2-user/bastion_ip)
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$bastion_ip \'
keypair=BajamsKey.pem
priv_ip_master1=$(cat /home/ec2-user/master0_ip)
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$priv_ip_master1 /bin/bash << _EOF
curl -L https://github.com/kubernetes/kompose/releases/download/v1.22.0/kompose-linux-amd64 -o kompose
sudo chmod +x kompose
sudo mv kompose /usr/local/bin/kompose
_EOF
\''''    
            }
        }
        stage('Deploy metrics server') {
            steps {
            sh '''keypair=BajamsKey.pem
bastion_ip=$(cat /home/ec2-user/bastion_ip)
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$bastion_ip \'
keypair=BajamsKey.pem
priv_ip_master1=$(cat /home/ec2-user/master0_ip)
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$priv_ip_master1 /bin/bash << _EOF
sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf
kubectl apply -f /home/ec2-user/assessment4bjms/metrics-server/metrics.yaml
_EOF
\''''    
            }
        }
    }
}
