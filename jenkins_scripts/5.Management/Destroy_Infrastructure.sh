#!/bin/bash
# Delete extra route 53s created for new blogs
keypair=BajamsKey.pem
bastion_ip=$(cat /home/ec2-user/bastion_ip)
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$bastion_ip '
keypair=BajamsKey.pem
priv_ip_master1=$(cat /home/ec2-user/master0_ip)
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$priv_ip_master1 /bin/bash << _EOF
sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf
cd /home/ec2-user/assessment4bjms/destroy
chmod +x delete_blogs.sh
./delete_blogs.sh
_EOF
'

# Terraform destroy rest of infrastructure
cd /home/ec2-user/workspace/Terraform_setup/Terraform
terraform destroy -auto-approve
cd ..
#git add . 
#git commit -m "infrastructure destroyed"
#git push origin master