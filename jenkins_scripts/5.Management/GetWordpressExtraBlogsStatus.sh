#!/bin/bash
# Curls IPs of known blogs, to get the status extra wordpress blogs
sudo touch /home/ec2-user/installedhosts /home/ec2-user/readytoinstallhosts
:>/home/ec2-user/installedhosts
:>/home/ec2-user/readytoinstallhosts
sudo chown ec2-user:ec2-user /home/ec2-user/installedhosts
sudo chown ec2-user:ec2-user /home/ec2-user/readytoinstallhosts

first_host=http://bajams-wordpress.academy.labs.automationlogic.com/
curl -I $first_host | grep 200
exitcode=$?
echo $exitcode
if [[ "$exitcode" == *"0"* ]]; then
   echo "$first_host\n" >> /home/ec2-user/installedhosts
fi

first_host=http://bajams-wordpress.academy.labs.automationlogic.com/wp-admin/install.php
curl -I $first_host | grep 200
exitcode=$?
echo $exitcode
if [[ "$exitcode" == *"0"* ]]; then
   echo "$first_host\n" >> /home/ec2-user/readytoinstallhosts
fi

i=0
exitcode=0
while [[ "$exitcode" == "0" ]];do
    hostname=http://bajams-wordpress$i-wp.academy.labs.automationlogic.com/
    echo $hostname
    curl -I $hostname | grep 200
    exitcode=$?
    echo $exitcode
    if [[ "$exitcode" == *"0"* ]]; then
    	echo "$hostname\n" >> /home/ec2-user/installedhosts
    fi
    let i++
done

i=0
exitcode=0
while [[ "$exitcode" == "0" ]];do
    hostname=http://bajams-wordpress$i-wp.academy.labs.automationlogic.com/wp-admin/install.php
    echo $hostname
    curl -I $hostname | grep 200
    exitcode=$?
    echo $exitcode
    if [[ "$exitcode" == *"0"* ]]; then
    	echo "$hostname\n" >> /home/ec2-user/readytoinstallhosts
    fi
    let i++
done

newblog="New Blog(s) available at:\n $(cat /home/ec2-user/readytoinstallhosts)"
installedblogs="Check out the installed blogs at:\n $(cat /home/ec2-user/installedhosts)"
cd /home/ec2-user/workspace/Slack_notifications/
python3 slacker.py $newblog
python3 slacker.py $installedblogs
#jobname=$(tail -n 1 /home/ec2-user/knownhosts)
#echo $jobname