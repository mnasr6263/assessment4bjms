#!/bin/bash
keypair=BajamsKey.pem
bastion_ip=$(cat /home/ec2-user/bastion_ip)

ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$bastion_ip '

keypair=BajamsKey.pem
priv_ip_master1=$(cat /home/ec2-user/master0_ip)

ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$priv_ip_master1 /bin/bash << EOF
sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf
kubectl rollout restart deploy -n petclinic
EOF
'