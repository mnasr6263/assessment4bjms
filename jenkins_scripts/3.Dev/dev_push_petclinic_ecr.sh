#!/bin/bash

sudo chmod 666 /var/run/docker.sock
aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 242392447408.dkr.ecr.eu-west-1.amazonaws.com
docker tag dev_test_petclinic_petclinic:latest 242392447408.dkr.ecr.eu-west-1.amazonaws.com/bajams-petclinic:latest
docker push 242392447408.dkr.ecr.eu-west-1.amazonaws.com/bajams-petclinic:latest