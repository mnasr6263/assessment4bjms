#!/bin/bash
# Builds Static App Dev Image, Run "Tests" to be approved by developer(s)
docker image prune -f
docker build -t dev_test_petclinic_petclinic:latest .
cd dev_testing
sudo docker-compose down
./test_image.sh $DBServerName $DBUserName $DBPassword $DBTable


