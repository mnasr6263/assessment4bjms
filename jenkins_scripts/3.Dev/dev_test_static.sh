#!/bin/bash
# Builds Static App Dev Image, Run "Tests" and pushes to ECR
sudo yum -y install docker
sudo systemctl start docker
sudo chmod 666 /var/run/docker.sock
docker build -t bajams-static .
docker tag bajams-static:latest 242392447408.dkr.ecr.eu-west-1.amazonaws.com/bajams-static:latest
echo "ALL TESTS PASSED - SUCCESSFUL BUILD OF STATIC DOCKER IMAGE"
aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 242392447408.dkr.ecr.eu-west-1.amazonaws.com
echo "PUSHING TO PRIVATE REPO!"
docker push 242392447408.dkr.ecr.eu-west-1.amazonaws.com/bajams-static:latest