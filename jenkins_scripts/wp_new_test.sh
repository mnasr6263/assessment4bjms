#!/bin/bash 

sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf

#This loop checks if wordpressi exists and increments the value of "i" if it does, so that a variable can be assigned to $newname
name=wordpress
kubectl get namespace | grep "$name"
namespaceexitcode=$?
i=0
while [[ $namespaceexitcode == "0" ]]; do
   newname=$name$i
   kubectl get namespace | grep "$newname"
   namespaceexitcode=$?
   let i++
done
kubectl create namespace $newname

sudo yum update
sudo yum install mariadb-server -y

# create a new database 
mysql -h bajams-database-wp.c6jtmnlypkzc.eu-west-1.rds.amazonaws.com -u admin -passessment4! -e "CREATE DATABASE IF NOT EXISTS $newname;"

# cd into wordpress directory
cd wordpress

# create new directory and copy original k8 files into it, whilst assigning them new names
mkdir $newname 
sudo cp k8/wp-deploy.yaml $newname/wp-$newname-deploy.yaml
sudo cp k8/wp-service.yaml $newname/wp-$newname-service.yaml
sudo cp k8/wp-ingress.yaml $newname/wp-$newname-ingress.yaml

# replace namespace with $newname 
cd $newname
sudo sed -i "s/  namespace: wordpress/  namespace: $newname/" wp-$newname-deploy.yaml
sudo sed -i "s/  namespace: wordpress/  namespace: $newname/" wp-$newname-service.yaml 
sudo sed -i "s/  namespace: wordpress/  namespace: $newname/" wp-$newname-ingress.yaml
sudo sed -i "s/          value: wordpress/          value: $newname/" wp-$newname-deploy.yaml  

# check if newname is within the current list of route53 names
aws route53 list-resource-record-sets --hosted-zone-id Z03386713MCJ0LHJCA6DL | grep $newname
routeexitcode=$?
if [ $routeexitcode -eq "0" ]
then
    echo "This Route53 name already exists, no need to run the aws command"
else
    echo '{
                "Comment": "CREATE a record ",
                "Changes": [{
                "Action": "CREATE",
                            "ResourceRecordSet": {
                                        "Name": "bajams-'$newname'-wp.academy.labs.automationlogic.com",
                                        "Type": "A",
                                        "AliasTarget": {"HostedZoneId":"Z32O12XQLNTSW2","DNSName": "Bajams-loadbalancer-329416162.eu-west-1.elb.amazonaws.com", "EvaluateTargetHealth": true}
    }}]
    }' > $newname.json

    aws route53 change-resource-record-sets --hosted-zone-id Z03386713MCJ0LHJCA6DL --change-batch file:///home/ec2-user/assessment4bjms/wordpress/$newname/$newname.json
fi

# replace host name in ingress.yaml 
sudo sed -i 's/"bajams-wordpress.academy.labs.automationlogic.com"/"bajams-'$newname'-wp.academy.labs.automationlogic.com"/' wp-$newname-ingress.yaml

# apply the new K8 files
kubectl apply -f wp-$newname-deploy.yaml,wp-$newname-ingress.yaml,wp-$newname-service.yaml 
kubectl apply -f /home/ec2-user/kubernetes-ingress/deploy/haproxy.yaml -n haproxy-controller

# Autoscale the deployments
kubectl autoscale deployment wordpress-deployment --cpu-percent=50 --min=1 --max=10 -n $newname

# Keep a record of which blogs are up
echo "$newname" >> /home/ec2-user/known_blogs
cd

# move the $newname directory out of the repo so that it is saved in the k8 master node and not deleted if you run the BaJaMS Git Clone job. 

mv /home/ec2-user/assessment4bjms/wordpress/$newname /home/ec2-user/k8_files 
 