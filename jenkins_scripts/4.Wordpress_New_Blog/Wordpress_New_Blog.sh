#!/bin/bash 

## ssh into bastion
keypair=BajamsKey.pem
bastion_ip=$(cat /home/ec2-user/bastion_ip)
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$bastion_ip '

## ssh into k8-master
keypair=BajamsKey.pem
priv_ip_master1=$(cat /home/ec2-user/master0_ip)
ssh -i ~/.ssh/$keypair ec2-user@$priv_ip_master1 /bin/bash << _EOF

## make directory k8_files if it does not exist
mkdir -p k8_files

# cd into repo
cd assessment4bjms

git add .
git commit -m 'deployment of applications'

# checkout to dev-mohamed-wordpress
git checkout dev-mohamed-wordpress 

chmod +x jenkins_scripts/wp_new.sh
./jenkins_scripts/wp_new_test.sh

cd /home/ec2-user/assessment4bjms

git add . 
git commit -m 'new blog'

git checkout master


_EOF
'