pipeline {
    agent {label 'worker1'}

    stages {
        stage('SSH into K8 nodes to pass ssh key') {
            steps {
sh '''keypair=BajamsKey.pem
s3_bucket_id=$(cat /home/ec2-user/s3_bucket_id)
aws s3 cp s3://$s3_bucket_id/$keypair /home/ec2-user/.ssh/$keypair
chmod 400 /home/ec2-user/.ssh/$keypair
bastion_ip=$(cat /home/ec2-user/bastion_ip)

scp -o StrictHostKeyChecking=no -i ~/.ssh/$keypair /home/ec2-user/s3_bucket_id ec2-user@$bastion_ip:
scp -o StrictHostKeyChecking=no -i ~/.ssh/$keypair /home/ec2-user/master0_ip ec2-user@$bastion_ip:
scp -o StrictHostKeyChecking=no -i ~/.ssh/$keypair /home/ec2-user/master1_ip ec2-user@$bastion_ip:
scp -o StrictHostKeyChecking=no -i ~/.ssh/$keypair /home/ec2-user/nodeA_ip ec2-user@$bastion_ip:
scp -o StrictHostKeyChecking=no -i ~/.ssh/$keypair /home/ec2-user/nodeB_ip ec2-user@$bastion_ip:
scp -o StrictHostKeyChecking=no -i ~/.ssh/$keypair /home/ec2-user/nodeC_ip ec2-user@$bastion_ip:
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$bastion_ip \'
keypair=BajamsKey.pem
priv_ip_master1=$(cat /home/ec2-user/master0_ip)
priv_ip_master2=$(cat /home/ec2-user/master1_ip)
priv_ip_nodeA=$(cat /home/ec2-user/nodeA_ip)
priv_ip_nodeB=$(cat /home/ec2-user/nodeB_ip)
priv_ip_nodeC=$(cat /home/ec2-user/nodeC_ip)
s3_bucket_id=$(cat /home/ec2-user/s3_bucket_id)

scp -o StrictHostKeyChecking=no -i ~/.ssh/$keypair /home/ec2-user/s3_bucket_id ec2-user@$priv_ip_master1:
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$priv_ip_master1 /bin/bash << _EOF
s3_bucket_id=$(cat /home/ec2-user/s3_bucket_id)
aws s3 cp s3://$s3_bucket_id/sshkey.pub /home/ec2-user/.ssh/sshkey.pub 
cat /home/ec2-user/.ssh/sshkey.pub >> /home/ec2-user/.ssh/authorized_keys
_EOF

scp -o StrictHostKeyChecking=no -i ~/.ssh/$keypair /home/ec2-user/s3_bucket_id ec2-user@$priv_ip_master2:
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$priv_ip_master2 /bin/bash << EOF_
s3_bucket_id=$(cat /home/ec2-user/s3_bucket_id)
aws s3 cp s3://$s3_bucket_id/sshkey.pub /home/ec2-user/.ssh/sshkey.pub 
cat /home/ec2-user/.ssh/sshkey.pub >> /home/ec2-user/.ssh/authorized_keys
EOF_

scp -o StrictHostKeyChecking=no -i ~/.ssh/$keypair /home/ec2-user/s3_bucket_id ec2-user@$priv_ip_nodeA:
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$priv_ip_nodeA /bin/bash << __EOF
s3_bucket_id=$(cat /home/ec2-user/s3_bucket_id)
aws s3 cp s3://$s3_bucket_id/sshkey.pub /home/ec2-user/.ssh/sshkey.pub 
cat /home/ec2-user/.ssh/sshkey.pub >> /home/ec2-user/.ssh/authorized_keys 
__EOF

scp -o StrictHostKeyChecking=no -i ~/.ssh/$keypair /home/ec2-user/s3_bucket_id ec2-user@$priv_ip_nodeB:
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$priv_ip_nodeB /bin/bash << EOF__
s3_bucket_id=$(cat /home/ec2-user/s3_bucket_id)
aws s3 cp s3://$s3_bucket_id/sshkey.pub /home/ec2-user/.ssh/sshkey.pub 
cat /home/ec2-user/.ssh/sshkey.pub >> /home/ec2-user/.ssh/authorized_keys 
EOF__

scp -o StrictHostKeyChecking=no -i ~/.ssh/$keypair /home/ec2-user/s3_bucket_id ec2-user@$priv_ip_nodeC:
ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$priv_ip_nodeC /bin/bash << _EOF_
s3_bucket_id=$(cat /home/ec2-user/s3_bucket_id)
aws s3 cp s3://$s3_bucket_id/sshkey.pub /home/ec2-user/.ssh/sshkey.pub 
cat /home/ec2-user/.ssh/sshkey.pub >> /home/ec2-user/.ssh/authorized_keys 
_EOF_
\''''
            }
        }
        stage('Configuration of kubespray installation') {
            steps {
            sh '''keypair=BajamsKey.pem
            bastion_ip=$(cat /home/ec2-user/bastion_ip)
            
            ssh -o StrictHostKeyChecking=no -i ~/.ssh/$keypair ec2-user@$bastion_ip \'
            cd /home/ec2-user/kubespray
            export AWS_DEFAULT_PROFILE=Academy
            ansible-playbook -i /home/ec2-user/kubespray/inventory/mycluster/hosts.yaml --become --become-user=root /home/ec2-user/kubespray/cluster.yml
            \'
            '''
            }
        }
    }
}
