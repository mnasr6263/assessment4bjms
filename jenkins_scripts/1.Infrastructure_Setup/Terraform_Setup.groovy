pipeline {
    agent {label 'worker1'}

    stages {
        stage('git clone') {
            steps {
                cleanWs()
                git credentialsId: 'jenkins-master-private-key', url: 'git@bitbucket.org:mnasr6263/assessment4bjms.git'
            }
        }
        stage('Terraform started') {
            steps {
                sh 'echo "terraform build starting!"'
            }
        }
        stage('Building security groups') {
            steps {
                sh '''
                cd /home/ec2-user/workspace/Terraform_setup/Terraform
                terraform init
                terraform apply -target=module.security_groups -auto-approve
                '''
                sshagent(credentials : ['jenkins-master-private-key']) {
                sh '''
                git add .
                git commit -m "tfstate change after making security groups"
                git push origin master
                '''
                }
            }
        }
        stage('Creating RDS databases') {
            steps {
                sshagent(credentials : ['jenkins-master-private-key']) {
                sh '''
                cd /home/ec2-user/workspace/Terraform_setup/Terraform
                git pull origin master
                terraform init
                terraform apply -target=module.rds_database -auto-approve
                git add .
                git commit -m "tfstate change after creating RDS database for petclinic and wordpress"
                git push origin master
                '''
                }
            }
        }
        stage('S3 Bucket build') {
            steps {
                sshagent(credentials : ['jenkins-master-private-key']) {
                sh '''cd /home/ec2-user/workspace/Terraform_setup/Terraform
                git pull origin master
                terraform init
                terraform apply -target=module.s3 -auto-approve
                git add .
                git commit -m "tfstate change after s3 bucket build"
                git push origin master
                '''
                }
            }
        }
        stage('Loadbalancer build and route 53 DNS names for webservers') {
            steps {
                sshagent(credentials : ['jenkins-master-private-key']) {
                sh '''cd /home/ec2-user/workspace/Terraform_setup/Terraform
                git pull origin master
                terraform init
                terraform apply -target=module.loadbalancer -auto-approve
                git add .
                git commit -m "tfstate change after loadbalancer build"
                git push origin master
                ''' 
               } 
            }
        }
        stage('Building Kubernetes Cluster') {
            steps {
                sshagent(credentials : ['jenkins-master-private-key']) {
                sh '''cd /home/ec2-user/workspace/Terraform_setup/Terraform
                git pull origin master
                terraform init
                terraform apply -target=module.k8_clusters -auto-approve
                git add .
                git commit -m "tfstate change after kubernetes cluster build"
                git push origin master
                '''
                }
            }
        }
        stage('Allow instances to initialise') {
            steps {
                sh'''
                echo "Sleep for 3 minutes while instances initialise before provisioning Kubernetes cluster"
                sleep 180
                '''
            }
        }
    }
}
